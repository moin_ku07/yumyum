//
//  CartTableViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 2/21/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreData

protocol CartTableViewControllerDelegate{
    func onMenuButtonTap()
}

class CartTableViewController: UITableViewController, PayPalPaymentDelegate, CartTableViewCellDelegate {
    
    var delegate: CartTableViewControllerDelegate?
    
    // PayPal configurations
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    
    // set paypal environment
    var paypalSandbox: Bool = false
    
    var acceptCreditCards: Bool = true {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    // ^PayPal config end
    
    var activityIndicatorParentView: AnyObject!
    var noDataMessageLabel: UILabel!
    var order_items: NSMutableArray = NSMutableArray()
    
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var restaurantNameLabel: UILabel!
    @IBOutlet var subtotalPrice: UILabel!
    @IBOutlet var shippingView: UIView!
    @IBOutlet var shippingLabel: UILabel!
    @IBOutlet var shippingPrice: UILabel!
    @IBOutlet var taxView: UIView!
    @IBOutlet var taxLabel: UILabel!
    @IBOutlet var taxPrice: UILabel!
    @IBOutlet var gTotalView: UIView!
    @IBOutlet var gtotalLabel: UILabel!
    @IBOutlet var gtotalPrice: UILabel!
    var tableData: NSMutableArray = NSMutableArray()
    
    var restaurant_name: String = "Undefined Restaurant"
    var tax: NSString = "0.0"
    let shipping: NSString = "0"
    var grandTotal: Float = 0.0
    var grandTotalValue: Float = 0.0
    var subTotalValue: Float = 0.0
    var calculatedTax: Float = 0.0
    
    var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let taxString: NSString = prefs.valueForKey("tax") as? NSString{
            tax = taxString
        }
        if let restaurantName: String = prefs.valueForKey("restaurant_name") as? String{
            self.restaurant_name = restaurantName
        }
        restaurantNameLabel.text = restaurant_name
        print("currentCartRestaurantTax: \(tax)")
        
        let indicatorSuperview = self.parentViewController?.view.window != nil ? self.parentViewController?.view.window! : (self.view.window != nil ? self.view.window! : self.view)
        if activityIndicatorParentView == nil{
            activityIndicatorParentView = indicatorSuperview
        }
        
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "ff3b30")
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(20)]
        
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.title = "Cart"
        self.navigationItem.hidesBackButton = true

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: "onMenuTap:")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Checkout", style: UIBarButtonItemStyle.Plain, target: self, action: "onCheckoutTap:")
        
        // Paypal preconfiguration
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = acceptCreditCards;
        payPalConfig.merchantName = "Yum Yum To Go"
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0] 
        
        self.tableView.registerNib(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        //self.tableView.separatorInset = UIEdgeInsetsZero
        self.tableView.separatorStyle = .None
        
        if tax.floatValue > 0{
            grandTotal += tax.floatValue
        }
        if shipping.floatValue > 0{
            grandTotal += shipping.floatValue
        }
        
        self.loadTableData()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //environment = PayPalEnvironmentProduction
        environment = paypalSandbox ? PayPalEnvironmentSandbox : PayPalEnvironmentProduction
        PayPalMobile.preconnectWithEnvironment(environment)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.tableData.count == 0{
            self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width,
                self.tableView.bounds.size.height))
            
            self.noDataMessageLabel.text = "No data is avaible"
            //center the text
            self.noDataMessageLabel.textAlignment = NSTextAlignment.Center
            //auto size the text
            self.noDataMessageLabel.sizeToFit()
            //set back to label view
            self.tableView.backgroundView = self.noDataMessageLabel
            return 0
        }else{
            if self.noDataMessageLabel != nil{
                self.noDataMessageLabel.removeFromSuperview()
            }
        }
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: CartTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CartTableViewCell
        
        let cellData: NSDictionary = self.tableData.objectAtIndex(indexPath.row) as! NSDictionary

        cell.data = cellData
        cell.delegate = self
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let cellData: NSDictionary = self.tableData.objectAtIndex(indexPath.row) as! NSDictionary
            print(cellData)
            var shouldReload: Bool = false
            var quantity: Float?, price: Float?
            if let q: NSString = cellData.objectForKey("quantity") as? NSString{
                quantity = q.floatValue
            }
            if let p: NSString = cellData.objectForKey("menu_price") as? NSString{
                price = p.floatValue
                if let addons: NSArray = cellData.objectForKey("addons") as? NSArray{
                    shouldReload = true
                }
            }else if let p: NSString = cellData.objectForKey("price") as? NSString{
                price = p.floatValue
            }
            if quantity != nil && price != nil{
                grandTotal = grandTotal - (quantity! * price!)
                gtotalPrice.text = "$\(grandTotal)"
            }
            if shouldReload{
                if let id: NSString = cellData.objectForKey("id") as? NSString{
                    print("id: \(id)")
                    let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("database")
                    let predicate: NSPredicate = NSPredicate(format: "id == '\(id)'")
                    let result: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: predicate, andSorter: nil, managedObjectContext: moc, limit: 1)
                    //println(result)
                    if result.count > 0{
                        let cart: Carts = result.lastObject as! Carts
                        moc.deleteObject(cart)
                        var error: NSError? = nil
                        do {
                            try moc.save()
                        } catch var error1 as NSError {
                            error = error1
                        }
                        if error != nil{
                            print(error?.localizedDescription)
                        }else{
                            print("record deleted")
                        }
                    }
                }
                self.loadTableData()
            }else{
                tableData.removeObjectAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            self.showHeaderFooterViews()
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - CartTableViewCellDelegate
    func onQuantityChange(indexPath: NSIndexPath, oldValue: Int, newValue: Int, price: Float) {
        grandTotal = grandTotal - (Float(oldValue) * price)
        grandTotal = grandTotal + (Float(newValue) * price)
        gtotalPrice.text = "$\(grandTotal)"
        print("grandTotal: \(grandTotal)")
        if let dict: NSMutableDictionary = self.tableData.objectAtIndex(indexPath.row) as? NSMutableDictionary{
            //println("here")
            if let q: NSString = dict.objectForKey("quantity") as? NSString{
                //println("here1")
                dict.setValue(String(newValue), forKey: "quantity")
                self.tableData.replaceObjectAtIndex(indexPath.row, withObject: dict)
                self.tableView.reloadData()
                //println("here2")
            }
            //println(dict)
            if let id: NSString = dict.objectForKey("id") as? NSString{
                //println("id: \(id)")
                let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("database")
                let predicate: NSPredicate = NSPredicate(format: "id == '\(id)'")
                let result: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: predicate, andSorter: nil, managedObjectContext: moc, limit: 1)
                //println(result)
                if result.count > 0{
                    let cart: Carts = result.lastObject as! Carts
                    cart.quantity = "\(newValue)"
                    /*let addons: NSArray = []
                    if let addonData: NSData = NSJSONSerialization.dataWithJSONObject(addons, options: NSJSONWritingOptions.allZeros, error: nil){
                        cart.addons = addonData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.allZeros)
                    }else{
                        cart.addons = "[]"
                    }*/
                    var error: NSError?
                    do {
                        try moc.save()
                    } catch var error1 as NSError {
                        error = error1
                    }
                    if error == nil{
                        print("updated")
                    }else{
                        print(error!.localizedDescription)
                    }
                }
            }
        }
        self.showHeaderFooterViews()
    }
    
    // MARK: - onCheckoutTap
    func onCheckoutTap(sender: UIBarButtonItem){
        
        /*let itme1Dict: NSDictionary = ["menu_name": "Menu 56","menu_id": "1","quantity": "2","menu_price": "5.00", "addons": [["id": "1","name": "Addon 1","price": "78.00","description": "Addon desc"]]]
        let itme2Dict: NSDictionary = ["menu_name": "Menu from csv","menu_id": "2","quantity": "1","menu_price": "9.99"]
        order_items = [itme1Dict, itme2Dict]*/
        order_items = NSMutableArray()
        
        let items: NSMutableArray = NSMutableArray()
        for item in self.tableData{
            if let itemDict: NSDictionary = item as? NSDictionary{
                var name: String!
                if let n: String = itemDict.objectForKey("menu_name") as? String{
                    name = n
                }else if let n: String = itemDict.objectForKey("name") as? String{
                    name = n
                }
                let quantity: UInt = UInt((itemDict.objectForKey("quantity") as! NSString).intValue)
                if quantity < 1{
                    continue
                }
                var price: String!
                if let p: String = itemDict.objectForKey("menu_price") as? String{
                    price = p
                }else if let p: String = itemDict.objectForKey("price") as? String{
                    price = p
                }
                var sku: String!
                if let s: String = itemDict.objectForKey("menu_id") as? String{
                    sku = "menu_" + s
                }else if let s: String = itemDict.objectForKey("id") as? String{
                    sku = "addon_" + s
                }
                //println("price: \(price)")
                
                order_items.addObject(["name": name, "quantity": quantity, "price": price, "id": sku])
                let pItem: PayPalItem = PayPalItem(name: name, withQuantity: quantity, withPrice: NSDecimalNumber(string: price), withCurrency: "USD", withSku: sku)
                items.addObject(pItem)
                
                /*if let addons: NSArray = itemDict.objectForKey("addons") as? NSArray{
                    for addon in addons{
                        if let addonDict: NSDictionary = addon as? NSDictionary{
                            let aName: String = addonDict.objectForKey("name") as String
                            let aQuantity: UInt = 1
                            let aPrice: String = addonDict.objectForKey("price") as String
                            let aSku: String = "addon_" + (addonDict.objectForKey("id") as String)
                            println("aPrice: \(aPrice)")
                            
                            let paItem: PayPalItem = PayPalItem(name: aName, withQuantity: aQuantity, withPrice: NSDecimalNumber(string: aPrice), withCurrency: "USD", withSku: aSku)
                            items.addObject(paItem)
                        }
                    }
                }*/
            }
        }
        // Optional: include multiple items
        //var item1 = PayPalItem(name: "Menu 56", withQuantity: 2, withPrice: NSDecimalNumber(string: "5.00"), withCurrency: "USD", withSku: "menu_1")
        //var item2 = PayPalItem(name: "Addon 1", withQuantity: 1, withPrice: NSDecimalNumber(string: "2.99"), withCurrency: "USD", withSku: "addon_1")
        //var item3 = PayPalItem(name: "Menu from csv", withQuantity: 2, withPrice: NSDecimalNumber(string: "9.99"), withCurrency: "USD", withSku: "menu_2")
        
        //let items = [item1, item2, item3]
        let subtotal = PayPalItem.totalPriceForItems(items as [AnyObject])
        
        // Optional: include payment details
        let shipping: NSDecimalNumber = NSDecimalNumber(string: self.shipping as String)
        let tax: NSDecimalNumber = NSDecimalNumber(string: NSString(format: "%.2f", self.calculatedTax) as String)
        print("shipping: \(shipping)")
        print("tax: \(tax)")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
        
        let payment: PayPalPayment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "\(restaurant_name)", intent: .Sale)
        
        payment.items = items as [AnyObject]
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            presentViewController(paymentViewController, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
            AlertManager.showAlert(self, title: "Error", message: "Payment not processalbe at this moment. Please check internet connectivity and try again.", buttonNames: nil, completion: nil)
        }
    }
    
    func verifyC(sender: UIBarButtonItem){
        let activityIndicator = UICustomActivityView()
        activityIndicator.message = "Confirming..."
        activityIndicator.showActivityIndicator(activityIndicatorParentView, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        //let postData: NSDictionary = ["Invoice": ["payment_id": payment_id, "restaurant_id": currentCartRestaurantId!, "order_info": self.order_items, "subtotal": ceilf((subTotalValue * 100) / 100), "totaltax": ceilf((calculatedTax * 100) / 100), "grandtotal": ceilf((grandTotal * 100) / 100)]]
        
        let postData: NSDictionary = ["Invoice": ["payment_id": "PAY-03598633HL7208311KVNQUEA", "restaurant_id": 1, "order_info": [["id" : "menu_1", "name" : "Menu 56", "price" : "0.01", "quantity" : 1], ["id" : "addon_1", "name" : "Addon 1", "price" : "0.01", "quantity" : 1], ["id" : "menu_4", "name" : "Menu from csv 3", "price" : "0.01", "quantity" : 1]], "subtotal": 0.03, "totaltax": 0.01, "grandtotal": 0.04]]
        print(postData)
        
        DataManager.postDataAsyncWithCallback("invoices/verify_payment", data: postData, json: true) { (data, error) -> Void in
            print("api/invoices/verify_payment")
            
            dispatch_async(dispatch_get_main_queue()){
                activityIndicator.hideActivityIndicator()
                if error != nil{
                    print(error)
                    AlertManager.showAlert(self, title: "Error", message: error!.localizedDescription, buttonNames: nil, completion: nil)
                }else if (data != nil){
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    
                    if let verifyResponse: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSDictionary{
                        if verifyResponse.objectForKey("success") as! Bool == true{
                            self.clearCart()
                            AlertManager.showAlert(self, title: "Success", message: verifyResponse.objectForKey("message") as! String!, buttonNames: nil, completion: { (index) -> Void in
                                self.loadTableData()
                            })
                        }else{
                            AlertManager.showAlert(self, title: "Error", message: verifyResponse.objectForKey("message") as! String!, buttonNames: nil, completion: nil)
                        }
                    }else{
                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    }
                }
                //println("======================")
                //println(data)
                //println(error)
            }
        }
    }
    
    func verifyCompletedPayment(completedPayment: PayPalPayment){
        // send completed confirmaion to your server
        //println("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
        //println("payment description:\n\n\(completedPayment.description)")
        //self.resultText = completedPayment.description
        
        let paymentDict: NSDictionary = NSDictionary(dictionary: completedPayment.confirmation)
        
        print("verifyCompletedPayment")
        
        if let response: NSDictionary = paymentDict.objectForKey("response") as? NSDictionary{
            if let payment_id: String = response.objectForKey("id") as? String{
                let activityIndicator = UICustomActivityView()
                activityIndicator.message = "Confirming..."
                activityIndicator.showActivityIndicator(activityIndicatorParentView, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
                let postData: NSDictionary = ["Invoice": ["payment_id": payment_id, "restaurant_id": currentCartRestaurantId!, "order_info": self.order_items, "subtotal": subTotalValue, "totaltax": calculatedTax, "grandtotal": grandTotalValue, "sandbox": paypalSandbox]]
                print(postData)
                
                DataManager.postDataAsyncWithCallback("invoices/verify_payment", data: postData, json: true) { (data, error) -> Void in
                    print("api/invoices/verify_payment")
                    
                    dispatch_async(dispatch_get_main_queue()){
                        activityIndicator.hideActivityIndicator()
                        if error != nil{
                            print(error)
                            AlertManager.showAlert(self, title: "Error", message: error!.localizedDescription, buttonNames: nil, completion: nil)
                        }else if (data != nil){
                            print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                            
                            if let verifyResponse: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSDictionary{
                                if verifyResponse.objectForKey("success") as! Bool == true{
                                    self.clearCart()
                                    AlertManager.showAlert(self, title: "Success", message: verifyResponse.objectForKey("message") as! String!, buttonNames: nil, completion: { (index) -> Void in
                                        self.loadTableData()
                                    })
                                }else{
                                    AlertManager.showAlert(self, title: "Error", message: verifyResponse.objectForKey("message") as! String!, buttonNames: nil, completion: nil)
                                }
                            }else{
                                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                            }
                        }
                        //println("======================")
                        //println(data)
                        //println(error)
                    }
                }
            }
        }
    }
    
    // MARK: - PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController!) {
        print("PayPal Payment Cancelled")
        resultText = ""
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController!, didCompletePayment completedPayment: PayPalPayment!) {
        print("PayPal Payment Success !")
        paymentViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.verifyCompletedPayment(completedPayment)
        })
    }
    
    // MARK: - Menu Button Tap
    
    func onMenuTap(sender: UIBarButtonItem){
        self.delegate?.onMenuButtonTap()
    }
    
    // MARK: - UIStatusBarStyle
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    // MARK: - loadTableData
    func loadServerCart()->NSMutableArray{
        var mutableArray: NSMutableArray = NSMutableArray()
        let activityIndicator = UICustomActivityView()
        activityIndicator.message = "Loading"
        activityIndicator.showActivityIndicator(activityIndicatorParentView, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        let userID: String = prefs.objectForKey("userID") as! String
        DataManager.loadDataAsyncWithCallback("carts/findCartDetails?user_id=\(userID)", completion: { (data, error) -> Void in
            //println(data)
            //println(error)
            dispatch_async(dispatch_get_main_queue()){
                activityIndicator.hideActivityIndicator()
                if error == nil && data != nil{
                    //println(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    if let response: NSArray = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSArray{
                        mutableArray = NSMutableArray(array: response)
                    }
                }else if error != nil{
                    AlertManager.showAlert(self, title: "Error", message: "\(error!.localizedDescription)", buttonNames: nil, completion: nil)
                }
            }
        })
        return mutableArray
    }
    
    // MARK: - Load Local Cart
    func loadLocalCart()-> NSMutableArray{
        let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("database")
        let results: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc)
        var mutableArray: NSMutableArray = NSMutableArray()
        print("here")
        for result in results{
            let cart: Carts = result as! Carts
            let cartDict: NSMutableDictionary = NSMutableDictionary()
            
            cartDict.setValue(cart.id, forKey: "id")
            cartDict.setValue(cart.menu_id, forKey: "menu_id")
            cartDict.setValue(cart.menu_name, forKey: "menu_name")
            cartDict.setValue(cart.menu_price, forKey: "menu_price")
            cartDict.setValue(cart.quantity, forKey: "quantity")
            if let addonsData: NSData = NSData(base64EncodedString: cart.addons, options: NSDataBase64DecodingOptions()){
                if let addonJSON: NSArray = (try? NSJSONSerialization.JSONObjectWithData(addonsData, options: NSJSONReadingOptions())) as? NSArray{
                    //println("id: \(cart.id), menu_id: \(cart.menu_id), menu_name: \(cart.menu_name), addons: \(NSString(data: addonsData, encoding: NSUTF8StringEncoding)!)")
                    //println("id: \(cart.id), menu_id: \(cart.menu_id), menu_name: \(cart.menu_name), addons: \(addonJSON)")
                    cartDict.setValue(addonJSON, forKey: "addons")
                }else{
                    print("fails to decode data")
                }
            }else{
                print("fails to decode addons base64")
            }
            
            mutableArray.addObject(cartDict)
        }
        return mutableArray
    }
    
    // MARK: - loadTableData
    func loadTableData(){
        self.tableData.removeAllObjects()
        self.processTableData(self.loadLocalCart())
        self.tableView.reloadData()
        self.showHeaderFooterViews()
    }
    
    // MARK: - Process Cart Data
    func processTableData(response: NSArray){
        self.grandTotal = 0
        if response.count > 0{
            for item in response{
                if let dict: NSDictionary = item as? NSDictionary{
                    self.tableData.addObject(NSMutableDictionary(dictionary: dict))
                    
                    var quantity: Float?, price: Float?
                    if let q: NSString = dict.objectForKey("quantity") as? NSString{
                        quantity = q.floatValue
                    }
                    if let p: NSString = dict.objectForKey("menu_price") as? NSString{
                        price = p.floatValue
                    }
                    if quantity != nil && price != nil{
                        //println("quantity: \(quantity!), price: \(price!), subtotal: \(quantity! * price!)")
                        self.grandTotal = self.grandTotal + (quantity! * price!)
                    }
                    quantity = nil
                    price = nil
                    if let addons: NSArray = dict.objectForKey("addons") as? NSArray{
                        let count: Int = addons.count
                        if count > 0{
                            for (index,addonDict) in addons.enumerate(){
                                let addonMDict: NSMutableDictionary = NSMutableDictionary(dictionary: addonDict as! NSDictionary)
                                addonMDict.setValue(true, forKey: "isAddon")
                                if let hasQuantity: String = addonMDict.objectForKey("quantity") as? String{
                                    if hasQuantity.isEmpty || hasQuantity == ""{
                                        if let quantity: String = dict.objectForKey("quantity") as? String{
                                            addonMDict.setValue(quantity, forKey: "quantity")
                                        }
                                    }
                                }
                                
                                self.tableData.addObject(addonMDict)
                                if let p: NSString = addonMDict.objectForKey("price") as? NSString{
                                    price = p.floatValue
                                }
                                if let q: NSString = addonMDict.objectForKey("quantity") as? NSString{
                                    quantity = q.floatValue
                                }
                                if quantity != nil && price != nil{
                                    //println("quantity: \(quantity!), price: \(price!), subtotal: \(quantity! * price!)")
                                    self.grandTotal = self.grandTotal + (quantity! * price!)
                                }
                            }
                        }
                    }
                }
            }
        }
        print("self.grandTotal: \(self.grandTotal)")
        
    }
    
    func showHeaderFooterViews(){
        if grandTotal <= 0 {
            headerView.hidden = true
            footerView.hidden = true
        }else{
            headerView.hidden = false
            footerView.hidden = false
            
            self.subtotalPrice.text = "$" + (NSString(format: "%.2f", self.grandTotal) as String)
            
            print("self.grandTotal before: \(self.grandTotal)")
            //self.grandTotal = ceilf(self.grandTotal * 100) / 100
            print("self.grandTotal after: \(self.grandTotal)")
            subTotalValue = self.grandTotal
            var subtotal: Float = self.grandTotal
            
            if tax.floatValue > 0{
                calculatedTax = subtotal * (tax.floatValue / 100)
                calculatedTax = ceilf(calculatedTax * 100) / 100
                taxPrice.text = "$" + ( NSString(format: "%.2f", calculatedTax) as String)
                subtotal = subtotal + calculatedTax
            }else{
                taxPrice.text = "$0.00"
                //taxView.removeFromSuperview()
            }
            
            if shipping.floatValue > 0{
                shippingPrice.text = "$" + (shipping as String)
                subtotal = subtotal + shipping.floatValue
            }else{
                shippingView.removeFromSuperview()
            }
            
            //grandTotalValue = ceilf(subtotal * 100) / 100
            grandTotalValue = subtotal
            
            gtotalPrice.text = "$" + (NSString(format: "%.2f", grandTotalValue) as String)
            
            print("subTotalValue: \(subTotalValue)")
            print("calculatedTax: \(calculatedTax)")
            print("grandTotalValue: \(grandTotalValue)")
        }
        
    }
    
    // MARK: - Clear Local Cart Items
    func clearCart() {
        let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("database")
        let results: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc)
        for result in results{
            let cart: Carts = result as! Carts
            moc.deleteObject(cart)
        }
        var error: NSError? = nil
        do {
            try moc.save()
        } catch var error1 as NSError {
            error = error1
        }
        if error != nil{
            print(error?.localizedDescription)
        }else{
            print("All record deleted")
        }
    }

}
