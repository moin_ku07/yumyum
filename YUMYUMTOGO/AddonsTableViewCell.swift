//
//  AddonsTableViewCell.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 2/11/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

class AddonsTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var switchView: UISwitch!
    @IBOutlet var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
