//
//  CoreDataHelper.swift
//  CoreDataPractice
//
//  Created by Moin Uddin on 10/30/14.
//  Copyright (c) 2014 Moin Uddin. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHelper: NSObject {
    
    class func directoryForDatabaseFilename()->NSString{
        return NSHomeDirectory().stringByAppendingString("/Library/PrivateData");
    }
    
    class func dataBaseFilename(name: String? = nil) ->NSString{
        if name != nil{
            return "\(name).sqlite";
        }
        return "database.sqlite";
    }
    
    class func managedObjectContext(dataBaseFilename: String? = nil) -> NSManagedObjectContext{
        var error: NSError? = nil;
        
        do {
            try NSFileManager.defaultManager().createDirectoryAtPath(CoreDataHelper.directoryForDatabaseFilename() as String, withIntermediateDirectories: true, attributes: nil)
        } catch let error1 as NSError {
            error = error1
        };
        
        if error != nil{
            print("Error: \(error?.localizedDescription)");
        }
        let path: NSString = "\(CoreDataHelper.directoryForDatabaseFilename())/\(CoreDataHelper.dataBaseFilename(dataBaseFilename))";
        //println("path: \(path)");
        
        let url: NSURL = NSURL(fileURLWithPath: path as String);
        
        let managedModel: NSManagedObjectModel = NSManagedObjectModel.mergedModelFromBundles(nil)!;
        
        let storeCoordinator: NSPersistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedModel);
        
        do {
            try storeCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch let error1 as NSError {
            error = error1
            print("Error: \(error?.localizedDescription)");
            abort();
        }
        
        let managedObjectContext: NSManagedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.MainQueueConcurrencyType);
        managedObjectContext.persistentStoreCoordinator = storeCoordinator;
        
        //println(managedObjectContext)
        
        return managedObjectContext;
    }
    
    class func insertManagedObject(className: NSString, managedObjectContext: NSManagedObjectContext) -> AnyObject{
        let managedObject: NSManagedObject = NSEntityDescription.insertNewObjectForEntityForName(className as String, inManagedObjectContext: managedObjectContext) ;
        return managedObject;
    }
    
    class func saveManagedObjectContext(managedObjectContext: NSManagedObjectContext) -> Bool{
        var error: NSError? = nil;
        do {
            try managedObjectContext.save()
            return true;
        } catch let error1 as NSError {
            error = error1
            print("Save Error: \(error?.localizedDescription)")
            return false;
        }
    }
    
    class func fetchEntities(className: NSString, withPredicate predicate: NSPredicate?, andSorter sorter: NSArray?, managedObjectContext: NSManagedObjectContext, limit: Int? = nil) -> NSArray{
        let fetchRequest: NSFetchRequest = NSFetchRequest();
        if limit != nil{
            fetchRequest.fetchLimit = limit!
        }
        let entityDescription: NSEntityDescription = NSEntityDescription.entityForName(className as String, inManagedObjectContext: managedObjectContext)!;
        fetchRequest.entity = entityDescription;
        
        if (predicate != nil){
            fetchRequest.predicate = predicate!
        }
        if sorter != nil{
            fetchRequest.sortDescriptors = sorter! as? [NSSortDescriptor]
        }
        
        fetchRequest.returnsObjectsAsFaults = false;
        let error: NSError? = nil;
        let items: NSArray = (try! managedObjectContext.executeFetchRequest(fetchRequest)) as NSArray;
        if error != nil{
            print("Fetch Error: \(error?.localizedDescription)");
            return [];
        }
        return items;
    }
   
}
