//
//  InvoiceDetailViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 5/18/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

class InvoiceDetailViewController: UIViewController {
    
    @IBOutlet var webview: UIWebView!
    var link: NSURL?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if link != nil{
            self.webview.loadRequest(NSURLRequest(URL: link!))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UIStatusBarStyle
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

}
