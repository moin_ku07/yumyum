//
//  RestaurantMapViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/25/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

protocol RestaurantMapViewControllerDelegate{
    func didSelectRowAtIndex(index: Int, rowdata: NSDictionary?)
}

class RestaurantMapViewController: UIViewController, GMSMapViewDelegate {
    
    var delegate: RestaurantMapViewControllerDelegate?
    
    var mapData: NSMutableArray?
    
    var mapView: GMSMapView!
    
    var markersArray: NSMutableArray = NSMutableArray()
    var fitBounds: GMSCoordinateBounds? = nil
    
    var mapCellView: MapCellView?
    
    var locationManager: CLLocationManager = CLLocationManager()
    var userLocation: CLLocation?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.view.backgroundColor = UIColor.redColor()

        mapView = GMSMapView()
        mapView.delegate = self
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(mapView)
        
        let viewsDict: [String: AnyObject] = ["mapView": mapView, "view" : self.view, "toplayoutGuide": self.topLayoutGuide, "bottomLayoutGuide": self.bottomLayoutGuide]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[mapView]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[toplayoutGuide]-0-[mapView]-0-[bottomLayoutGuide]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict))
        
        dispatch_async(dispatch_get_main_queue()){
            self.setupMarkers()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Setup Markers
    
    func setupMarkers(){
        if mapData?.count > 0{
            self.removeMarkers()
            fitBounds = nil
            for (index,value) in (mapData!).enumerate(){
                let restDict: NSDictionary = value as! NSDictionary
                let latString = restDict.objectForKey("lat") as! String
                let lngString = restDict.objectForKey("lng") as! String
                let latTrim: NSString = latString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                let lngTrim: NSString = lngString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                let lat: Double = latTrim.doubleValue
                let lng: Double = lngTrim.doubleValue
                let position: CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(lat), CLLocationDegrees(lng))
                //println(position)
                let marker: GMSMarker = GMSMarker(position: position)
                marker.map = self.mapView
                marker.userData = index
                //self.markersArray.addObject(marker)
                if fitBounds == nil{
                    fitBounds = GMSCoordinateBounds(coordinate: marker.position, coordinate: marker.position)
                }else{
                    fitBounds = fitBounds!.includingCoordinate(marker.position)
                }
            }
            //println(fitBounds)
            self.mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(self.fitBounds!, withPadding: 15.0))
        }
    }
    
    func removeMarkers(){
        self.mapView.clear()
        /*
        if markersArray.count > 0{
            for marker in markersArray{
                let nMarker: GMSMarker = marker as GMSMarker
                nMarker.map = nil
            }
            markersArray.removeAllObjects()
        }*/

    }
    
    // MARK: - GMSMapViewDelegate
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        self.mapCellView?.hidden = true
        self.mapCellView?.removeFromSuperview()
        let markerIndex: Int = marker.userData as! Int
        let markerData: NSDictionary = self.mapData!.objectAtIndex(markerIndex) as! NSDictionary
        //println(markerData)
        self.addMapCellView()
        mapCellView?.cellIndex = markerIndex
        mapCellView?.cellData = markerData
        mapCellView?.thumb = DataManager.domain().root + "img/restaurants/thumb/" + (markerData.objectForKey("thumb") as! String)
        mapCellView?.title = markerData.objectForKey("name") as? String
        mapCellView?.cuisine = markerData.objectForKey("cuisine") as? String
        if let typeText: String = markerData.objectForKey("type") as? String{
            mapCellView?.type = typeText
        }
        if self.userLocation != nil{
            let latString = markerData.objectForKey("lat") as! String
            let lngString = markerData.objectForKey("lng") as! String
            let latTrim: NSString = latString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let lngTrim: NSString = lngString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let lat: Double = latTrim.doubleValue
            let lng: Double = lngTrim.doubleValue
            let restaurantCoordinate: CLLocation? = CLLocation(latitude: lat, longitude: lng)
            if restaurantCoordinate != nil{
                if let distance: CLLocationDistance = restaurantCoordinate?.distanceFromLocation(CLLocation(latitude: self.userLocation!.coordinate.latitude, longitude: self.userLocation!.coordinate.longitude)){
                    let distanceInMile: Double = (distance / 1000) * 0.621371
                    let formatter: NSNumberFormatter = NSNumberFormatter()
                    formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                    formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
                    formatter.maximumFractionDigits = 2
                    mapCellView?.distance = "\(formatter.stringFromNumber(distanceInMile)!) MI"
                }
            }
        }
        
        UIView.transitionWithView(self.view, duration: 0.25, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { () -> Void in
            self.mapCellView!.hidden = false
            }, completion: nil)
        
        return false
    }
    
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        UIView.transitionWithView(self.view, duration: 0.25, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { () -> Void in
            self.mapCellView?.hidden = true
            self.mapCellView?.removeFromSuperview()
            }, completion: nil)
    }
    
    // MARK: - ObserverValueForKeyPath
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "myLocation"{
            let locationDict: NSDictionary = change! as NSDictionary
            userLocation = locationDict.objectForKey(NSKeyValueChangeNewKey) as? CLLocation
            
        }
    }
    
    // MARK: - Add MapCellView
    func addMapCellView(){
        mapCellView = MapCellView()
        mapCellView!.hidden = true
        mapCellView!.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(mapCellView!)
        self.view.addConstraint(NSLayoutConstraint(item: mapCellView!, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: mapCellView!, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: mapCellView!, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: mapCellView!, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 112))
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onMapCellTap:")
        tapGesture.numberOfTapsRequired = 1
        mapCellView!.userInteractionEnabled = true
        mapCellView!.addGestureRecognizer(tapGesture)
    }
    
    func onMapCellTap(gesture: UITapGestureRecognizer){
        delegate?.didSelectRowAtIndex(mapCellView!.cellIndex!, rowdata: mapCellView!.cellData!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
