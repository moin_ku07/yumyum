//
//  LoginViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/14/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreData

@objc protocol LoginViewControllerDelegate{
    optional func didLogggeIn(success: Bool)
    optional func onMenuButtonTap()
}

class LoginViewController: UIViewController, UITextFieldDelegate, RestaurantViewControllerDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var inputContainer: UIView!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var forgotLabel: UILabel!
    @IBOutlet var skipLogin: UIButton!
    @IBOutlet var signupButton: UIButton!
    
    var delegate: LoginViewControllerDelegate?
    
    var shouldHideOnLogin : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        
        // style inputContainer
        inputContainer.backgroundColor = UIColor.clearColor()
        inputContainer.layer.borderWidth = 1
        //inputContainer.layer.cornerRadius = 8
        inputContainer.layer.borderColor = UIColor(rgba: "#b7b9bd").CGColor
        
        //style inputfield
        usernameTextField.delegate = self
        usernameTextField.leftViewMode = UITextFieldViewMode.Always
        usernameTextField.leftView = UIView(frame: CGRectMake(0, 0, 5, usernameTextField.frame.size.height))
        passwordTextField.delegate = self
        passwordTextField.leftViewMode = UITextFieldViewMode.Always
        passwordTextField.leftView = UIView(frame: CGRectMake(0, 0, 5, passwordTextField.frame.size.height))
        
        // style login button
        loginButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        loginButton.setTitleColor(UIColor(rgba: "333333"), forState: UIControlState.Highlighted)
        loginButton.layer.borderColor = UIColor(rgba: "#b7b9bd").CGColor
        //loginButton.layer.cornerRadius = 8
        loginButton.layer.borderWidth = 1
        loginButton.layer.masksToBounds = true
        let delay: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, 10 * Int64(NSEC_PER_MSEC))
        dispatch_after(delay, dispatch_get_main_queue()) {
            let btnGradient: CAGradientLayer = CAGradientLayer()
            btnGradient.frame = self.loginButton.bounds
            btnGradient.colors = [UIColor(rgba: "f1f1f1").CGColor, UIColor(rgba: "d9d9d9").CGColor, UIColor(rgba: "d1d1d1").CGColor, UIColor(rgba: "c0c0c0").CGColor]
            btnGradient.locations = [0.0, 0.5, 0.5, 1.0]
            //self.loginButton.layer.insertSublayer(btnGradient, atIndex: 0)
        }
        
        // style signupButton
        signupButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        signupButton.setTitleColor(UIColor(rgba: "333333"), forState: UIControlState.Highlighted)
        signupButton.layer.borderColor = UIColor(rgba: "#b7b9bd").CGColor
        //signupButton.layer.cornerRadius = 8
        signupButton.layer.borderWidth = 1
        signupButton.layer.masksToBounds = true
        dispatch_after(delay, dispatch_get_main_queue()) {
            let btnGradient: CAGradientLayer = CAGradientLayer()
            btnGradient.frame = self.signupButton.bounds
            btnGradient.colors = [UIColor(rgba: "f1f1f1").CGColor, UIColor(rgba: "d9d9d9").CGColor, UIColor(rgba: "d1d1d1").CGColor, UIColor(rgba: "c0c0c0").CGColor]
            btnGradient.locations = [0.0, 0.5, 0.5, 1.0]
            //self.signupButton.layer.insertSublayer(btnGradient, atIndex: 0)
        }
        
        // tap gesture
        let viewTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onViewTap:")
        scrollView.addGestureRecognizer(viewTapGesture)
        // tap gesture
        let forgotTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onForgotTap:")
        forgotTapGesture.numberOfTapsRequired = 1
        forgotLabel.addGestureRecognizer(forgotTapGesture)
        
        //usernameTextField.text = "moinku07@gmail.com"
        //passwordTextField.text = "123456"
        
        if shouldHideOnLogin == true{
            forgotLabel.hidden = true
            //skipLogin.hidden = true
            signupButton.hidden = true
            self.skipLogin.setAttributedTitle(NSAttributedString(string: "Cancel"), forState: UIControlState.Normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Textfield delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        let scrollPoint: CGPoint = CGPointMake(0, textField.frame.origin.y)
        self.scrollView.setContentOffset(scrollPoint, animated: true)
    }
    
    func textViewDidEndEditing(textField: UITextField) {
        self.scrollView.setContentOffset(CGPointZero, animated: true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag: Int = textField.tag + 1
        
        //println("nextTag: \(nextTag)")
        
        if let nextResponder: UIResponder = textField.superview?.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            self.onViewTap(self.view)
        }
        
        if textField.tag == 2{
            self.loginButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }
        
        return false
    }
    
    // MARK: - ContainerView tap handle
    
    func onViewTap(sender: UIView){
        usernameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        self.scrollView.setContentOffset(CGPointZero, animated: true)
    }
    
    func registerPushNotification(){
        if #available(iOS 8.0, *) {
            let notificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
            UIApplication.sharedApplication().registerForRemoteNotifications()
        } else {
            UIApplication.sharedApplication().registerForRemoteNotificationTypes([UIRemoteNotificationType.Alert, UIRemoteNotificationType.Badge, UIRemoteNotificationType.Sound])
        }
    }

    // MARK: - Login Button Tap
    @IBAction func onLoginButtonTap(sender: UIButton) {
        usernameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        self.onViewTap(self.scrollView)
        
        if usernameTextField.text!.isEmpty || !FormValidation.isValidEmail(usernameTextField.text!){
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter valid email", buttonNames: ["Okay"], completion: nil)
        }else if passwordTextField.text!.isEmpty{
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter password", buttonNames: ["Okay"], completion: nil)
        }else{
            let activityIndicator = UICustomActivityView()
            activityIndicator.showActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
            
            let loginData: NSDictionary = ["User": ["email": usernameTextField.text!, "password": passwordTextField.text!]]
            
            //println(loginData)
            
            DataManager.postDataAsyncWithCallback("users/login", data: loginData, json: true, completion: { (data, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    activityIndicator.hideActivityIndicator()
                    if error == nil && data != nil{
                        let loginData = JSON(data: data!)
                        print(loginData)
                        
                        if loginData["success"] == true && loginData["login"] == true{
                            self.registerPushNotification()
                            isLoggedIn = true
                            let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                            if let olduser: String = prefs.objectForKey("oldusername") as? String{
                                //println(olduser)
                                if olduser != self.usernameTextField.text{
                                    self.clearCart()
                                }
                            }
                            prefs.setObject(self.usernameTextField.text, forKey: "username")
                            prefs.setObject(self.usernameTextField.text, forKey: "oldusername")
                            prefs.setObject(self.passwordTextField.text, forKey: "password")
                            prefs.setObject(loginData["user_id"].stringValue, forKey: "userID")
                            prefs.synchronize()
                            if self.shouldHideOnLogin == true{
                                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                                    self.delegate?.didLogggeIn!(true)
                                })
                            }else{
                                let vc: RestaurantViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                                vc.delegate = self
                                self.navigationController?.pushViewController(vc, animated: true)
                                if let nvc: NavigationController = self.navigationController as? NavigationController{
                                    nvc.updateSidebarMenu()
                                }
                            }
                        }else{
                            var message: String = "Email / Password incorrect. Please enter valid credentials."
                            if loginData["message"] != ""{
                                message = loginData["message"].stringValue
                            }
                            AlertManager.showAlert(self, title: "Error", message: "\(message)", buttonNames: nil)
                        }
                        
                    }else if let posterror = error{
                        AlertManager.showAlert(self, title: "Error", message: "Error code: \(posterror.code). \(posterror.localizedDescription)", buttonNames: nil)
                    }
                    //println(JSON(data: data!))
                    //println(error)
                })
            })
        }
    }
    
    // MARK: - Forgot Password
    func onForgotTap(gesture: UITapGestureRecognizer){
        let vc: ForgotViewController = self.storyboard?.instantiateViewControllerWithIdentifier("forgotViewController") as! ForgotViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Signup
    
    @IBAction func signupButtonTap(sender: UIButton) {
        let vc: SignupViewController = self.storyboard?.instantiateViewControllerWithIdentifier("signupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Skip Login
    
    @IBAction func onSkipLoginTap(sender: UIButton) {
        isLoggedIn = false
        if shouldHideOnLogin == true{
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                self.delegate?.didLogggeIn!(false)
            })
            return
        }
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        prefs.setObject(nil, forKey: "username")
        prefs.setObject(nil, forKey: "password")
        prefs.synchronize()
        let vc: RestaurantViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        if let nvc: NavigationController = self.navigationController as? NavigationController{
            nvc.updateSidebarMenu()
        }
    }
    
    // MARK: - onMenuButtonTap
    func onMenuButtonTap() {
        self.delegate?.onMenuButtonTap!()
    }
    
    // MARK: - Clear Local Cart Items
    func clearCart() {
        let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("database")
        let results: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc)
        for result in results{
            let cart: Carts = result as! Carts
            moc.deleteObject(cart)
        }
        var error: NSError? = nil
        do {
            try moc.save()
        } catch let error1 as NSError {
            error = error1
        }
        if error != nil{
            print(error?.localizedDescription)
        }else{
            print("All record deleted")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
