//
//  Carts.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 2/28/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import Foundation
import CoreData

@objc(Carts)
class Carts: NSManagedObject {

    @NSManaged var id: String
    @NSManaged var menu_name: String
    @NSManaged var menu_price: String
    @NSManaged var quantity: String
    @NSManaged var addons: String
    @NSManaged var restaurant_id: String
    @NSManaged var menu_id: String

}
