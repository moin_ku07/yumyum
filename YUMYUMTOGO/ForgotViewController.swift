//
//  ForgotViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/14/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

class ForgotViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var inputContainer: UIView!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var haveacLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true

        // style inputContainer
        inputContainer.backgroundColor = UIColor.clearColor()
        inputContainer.layer.borderWidth = 1
        //inputContainer.layer.cornerRadius = 8
        inputContainer.layer.borderColor = UIColor(rgba: "#b7b9bd").CGColor
        
        //style inputfield
        usernameTextField.delegate = self
        usernameTextField.leftViewMode = UITextFieldViewMode.Always
        usernameTextField.leftView = UIView(frame: CGRectMake(0, 0, 5, usernameTextField.frame.size.height))
        
        // style login button
        loginButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        loginButton.setTitleColor(UIColor(rgba: "333333"), forState: UIControlState.Highlighted)
        loginButton.layer.borderColor = UIColor(rgba: "#b7b9bd").CGColor
        //loginButton.layer.cornerRadius = 8
        loginButton.layer.borderWidth = 1
        loginButton.layer.masksToBounds = true
        let delay: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, 10 * Int64(NSEC_PER_MSEC))
        dispatch_after(delay, dispatch_get_main_queue()) {
            let btnGradient: CAGradientLayer = CAGradientLayer()
            btnGradient.frame = self.loginButton.bounds
            btnGradient.colors = [UIColor(rgba: "f1f1f1").CGColor, UIColor(rgba: "d9d9d9").CGColor, UIColor(rgba: "d1d1d1").CGColor, UIColor(rgba: "c0c0c0").CGColor]
            btnGradient.locations = [0.0, 0.5, 0.5, 1.0]
            //self.loginButton.layer.insertSublayer(btnGradient, atIndex: 0)
        }
        
        // tap gesture
        let viewTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onViewTap:")
        scrollView.addGestureRecognizer(viewTapGesture)
        
        // tap gesture
        let forgotTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onHaveacTap:")
        forgotTapGesture.numberOfTapsRequired = 1
        haveacLabel.addGestureRecognizer(forgotTapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Textfield delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        let scrollPoint: CGPoint = CGPointMake(0, textField.frame.origin.y)
        self.scrollView.setContentOffset(scrollPoint, animated: true)
    }
    
    func textViewDidEndEditing(textField: UITextField) {
        self.scrollView.setContentOffset(CGPointZero, animated: true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag: Int = textField.tag + 1
        
        //println("nextTag: \(nextTag)")
        
        if let nextResponder: UIResponder = textField.superview?.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            self.onViewTap(self.view)
        }
        
        if textField.tag == 1{
            self.loginButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }
        
        return false
    }
    
    // MARK: - ContainerView tap handle
    
    func onViewTap(sender: UIView){
        usernameTextField.resignFirstResponder()
        self.scrollView.setContentOffset(CGPointZero, animated: true)
    }
    
    // MARK: - Login Button Tap
    @IBAction func onLoginButtonTap(sender: UIButton) {
        self.onViewTap(self.scrollView)
        
        if usernameTextField.text!.isEmpty || !FormValidation.isValidEmail(usernameTextField.text!){
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter correct email.", buttonNames: ["Okay"], completion: nil)
        }else{
            let activityIndicator = UICustomActivityView()
            activityIndicator.showActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
            
            let loginData: NSDictionary = ["User": ["email": usernameTextField.text!]]
            
            print(loginData)
            
            DataManager.postDataAsyncWithCallback("users/password_reset", data: loginData, json: true, completion: { (data, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    activityIndicator.hideActivityIndicator()
                    if error == nil && data != nil{
                        let resetData: NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as! NSDictionary
                        let success: Bool? = resetData.objectForKey("success") as? Bool
                        if success == true{
                            var message: String = "An email has been sent with password reset instruction."
                            if let msg: String = resetData.objectForKey("message") as? String{
                                message = msg
                            }
                            AlertManager.showAlert(self, title: "Success", message: message, buttonNames: nil)
                        }else{
                            var message: String = "Invalid email"
                            if let msg: String = resetData.objectForKey("message") as? String{
                                message = msg
                            }
                            AlertManager.showAlert(self, title: "Error", message: "\(message)", buttonNames: nil)
                        }
                    }else if let posterror = error{
                        AlertManager.showAlert(self, title: "Error", message: "Error code: \(posterror.code). \(posterror.localizedDescription)", buttonNames: nil)
                    }
                    print(JSON(data: data!))
                    print(error)
                })
            })
        }
    }
    
    // MARK: - Have Account
    func onHaveacTap(gesture: UITapGestureRecognizer){
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
