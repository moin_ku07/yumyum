//
//  RestaurantViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/25/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

protocol RestaurantViewControllerDelegate{
    func onMenuButtonTap()
}

class RestaurantViewController: UIViewController, RestaurantTableViewControllerDelegate, MenusListViewControllerDelegate, RestaurantMapViewControllerDelegate {
    
    var delegate: RestaurantViewControllerDelegate?

    @IBOutlet var containerView: UIView!
    
    var mapBarButton: UIBarButtonItem!
    var listBarButton: UIBarButtonItem!
    
    var tableListData: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "ff3b30")
        self.navigationController?.navigationBar.translucent = false
        //UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: UIControlState.Highlighted)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        self.navigationController?.navigationBarHidden = false
        //self.navigationItem.title = "Yum Yum To Go"
        self.navigationItem.hidesBackButton = true
        // set back button for child view
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Restaurants", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        
        let titleImage: UIImageView = UIImageView(image: UIImage(named: "yumyumtitle"))
        //titleImage.contentMode = UIViewContentMode.ScaleAspectFit
        //titleImage.clipsToBounds = true
        self.navigationItem.titleView = titleImage
        
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: "onMenuTap:")
        
        mapBarButton = UIBarButtonItem()
        mapBarButton.image = UIImage(named: "icon-map")
        mapBarButton.action = Selector("onMapBarButtonTap:")
        mapBarButton.target = self
        listBarButton = UIBarButtonItem()
        listBarButton.image = UIImage(named: "icon-list")
        listBarButton.action = Selector("onListBarButtonTap:")
        listBarButton.target = self
        self.navigationItem.rightBarButtonItem = mapBarButton
        
        //let delay: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, 300 * Int64(NSEC_PER_MSEC))
        //dispatch_after(delay, dispatch_get_main_queue()){
        dispatch_async(dispatch_get_main_queue()){
            self.loadRestaurantListView()
            //self.loadRestaurantMapView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadRestaurantListView(){
        self.navigationItem.rightBarButtonItem = self.mapBarButton
        self.containerView.removeAllSubViews()
        let restaurantListVC: RestaurantTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantTableViewController") as! RestaurantTableViewController
        restaurantListVC.delegate = self
        restaurantListVC.view.frame = self.containerView.bounds
        self.containerView.addSubview(restaurantListVC.view)
        self.addChildViewController(restaurantListVC)
        restaurantListVC.didMoveToParentViewController(self)
    }
    
    func loadRestaurantMapView(){
        self.navigationItem.rightBarButtonItem = self.listBarButton
        self.containerView.removeAllSubViews()
        let restaurantMapVC: RestaurantMapViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantMapViewController") as! RestaurantMapViewController
        restaurantMapVC.delegate = self
        restaurantMapVC.mapData = self.tableListData
        restaurantMapVC.view.frame = self.containerView.bounds
        self.containerView.addSubview(restaurantMapVC.view)
        self.addChildViewController(restaurantMapVC)
        restaurantMapVC.didMoveToParentViewController(self)
    }
    
    func onMapBarButtonTap(sender: UIBarButtonItem){
        self.loadRestaurantMapView()
    }
    
    func onListBarButtonTap(sender: UIBarButtonItem){
        self.loadRestaurantListView()
    }
    
    func onMenuTap(sender: UIBarButtonItem){
        self.delegate?.onMenuButtonTap()
    }
    
    // MARK: - RestaurantTableViewControllerDelegate
    func onTableDataUpdate(tableData: NSMutableArray) {
        tableListData.removeAllObjects()
        tableListData = tableData
    }
    
    func didSelectRowAtIndex(index: Int, rowdata: NSDictionary?) {
        let vc: MenusListViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MenusListViewController") as! MenusListViewController
        vc.restaurantData = rowdata
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - MenusListViewControllerDelegate
    func didSelectMenuAtIndex(index: Int, rowdata: NSDictionary?, restaurantdata: NSDictionary?) {
        let vc: AddonsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddonsViewController") as! AddonsViewController
        vc.menuData = rowdata
        vc.restaurant_id = restaurantdata?.objectForKey("id") as? String
        vc.restaurant_name = restaurantdata?.objectForKey("name") as? String
        vc.taxValue = restaurantdata?.objectForKey("tax") as? String
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - UIStatusBarStyle
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView{
    func removeAllSubViews(){
        for subView :AnyObject in self.subviews{
            subView.removeFromSuperview()
        }
    }
}
