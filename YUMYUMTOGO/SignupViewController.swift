//
//  SignupViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/14/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var inputContainer1: UIView!
    @IBOutlet var firstnameField: UITextField!
    @IBOutlet var lastnameField: UITextField!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    @IBOutlet var addressField: UITextField!
    @IBOutlet var addressField2: UITextField!
    @IBOutlet var cityField: UITextField!
    @IBOutlet var stateField: UITextField!
    @IBOutlet var zipField: UITextField!
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var signupButton: UIButton!
    
    var inputsArray: NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.navigationBarHidden = false

        // style inputContainer
        inputContainer1.backgroundColor = UIColor.clearColor()
        //inputContainer1.layer.borderWidth = 1
        //inputContainer1.layer.cornerRadius = 8
        //inputContainer1.layer.borderColor = UIColor(rgba: "#b7b9bd").CGColor
        
        // style inputs
        inputsArray = [firstnameField, lastnameField,usernameField, passwordField, addressField, addressField2, cityField, stateField, zipField, phoneField]
        let inpArray1: NSArray = [firstnameField, lastnameField,usernameField, passwordField]
        let inpArray2: NSArray = [addressField, addressField2, cityField, stateField, zipField, phoneField]
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            for (index, input) in inpArray1.enumerate(){
                let inp: UITextField = input as! UITextField
                inp.delegate = self
                inp.leftViewMode = UITextFieldViewMode.Always
                inp.leftView = UIView(frame: CGRectMake(0, 0, 5, inp.frame.size.height))
                if index == inpArray1.count - 1{
                    self.addBottomBorder(inp, location: "all")
                }else{
                    self.addBottomBorder(inp)
                }
            }
            for (index, input) in inpArray2.enumerate(){
                let inp: UITextField = input as! UITextField
                inp.delegate = self
                inp.leftViewMode = UITextFieldViewMode.Always
                inp.leftView = UIView(frame: CGRectMake(0, 0, 5, inp.frame.size.height))
                if index == inpArray2.count - 1{
                    self.addBottomBorder(inp, location: "all")
                }else{
                    self.addBottomBorder(inp)
                }
            }
        })
        
        // tap gesture
        let viewTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onViewTap:")
        scrollView.addGestureRecognizer(viewTapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Add input bottom border
    
    func addBottomBorder(sender: UITextField, location: String? = nil){
        
        /*let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(rgba: "#b7b9bd").CGColor
        if location == nil{
            border.frame = CGRect(x: 0, y: sender.frame.size.height - width, width:  sender.frame.size.width, height: sender.frame.size.height)
        }else{
            border.frame = CGRect(x: 0, y: 0, width:  sender.frame.size.width, height: sender.frame.size.height)
        }
        
        border.borderWidth = width
        sender.layer.addSublayer(border)
        sender.layer.masksToBounds = true
        //sender.clipsToBounds = true
        */
        
        let layerFrame: CGRect = CGRectMake(0, 0, sender.frame.size.width, sender.frame.size.height)
        let path: CGMutablePathRef = CGPathCreateMutable()
        
        CGPathMoveToPoint(path, nil, 0, 0)
        CGPathAddLineToPoint(path, nil, sender.frame.size.width, 0)
        
        CGPathMoveToPoint(path, nil, sender.frame.size.width, 0)
        CGPathAddLineToPoint(path, nil, sender.frame.size.width, sender.frame.size.height)
        
        CGPathMoveToPoint(path, nil, 0, 0)
        CGPathAddLineToPoint(path, nil, 0, sender.frame.size.height)
        
        if location != nil{
            CGPathMoveToPoint(path, nil, 0, sender.frame.size.height)
            CGPathAddLineToPoint(path, nil, sender.frame.size.width, sender.frame.size.height)
        }
        
        let line: CAShapeLayer = CAShapeLayer()
        line.path = path
        line.lineWidth = 2
        line.frame = layerFrame
        line.strokeColor = UIColor(rgba: "#b7b9bd").CGColor
        sender.layer.addSublayer(line)
        sender.layer.masksToBounds = true
    }
    
    // MARK: - Textfield delegate
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField === zipField || textField === phoneField{
            // Create a button bar for the number pad
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            
            // Setup the buttons to be put in the system.
            var item: UIBarButtonItem = UIBarButtonItem()
            if textField === zipField{
                item = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Bordered, target: self, action: Selector("zipNextTap") )
            }else if textField === phoneField{
                item = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: Selector("phoneDoneTap") )
            }
            let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let toolbarButtons = [flexSpace,item]
            
            //Put the buttons into the ToolBar and display the tool bar
            keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
            textField.inputAccessoryView = keyboardDoneButtonView
        }
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        let scrollPoint: CGPoint = CGPointMake(0, textField.frame.origin.y + 10)
        self.scrollView.setContentOffset(scrollPoint, animated: true)
    }
    
    func textViewDidEndEditing(textField: UITextField) {
        self.scrollView.setContentOffset(CGPointZero, animated: true)
    }
    
    func zipNextTap(){
        let nextTag: Int = zipField.tag + 1
        if let nextResponder: UIResponder = zipField.superview?.superview?.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }else{
            zipField.resignFirstResponder()
            self.onViewTap(self.view)
        }
    }
    
    func phoneDoneTap(){
        let nextTag: Int = phoneField.tag + 1
        if let nextResponder: UITextField = phoneField.superview?.superview?.viewWithTag(nextTag) as? UITextField{
            nextResponder.becomeFirstResponder()
        }else{
            phoneField.resignFirstResponder()
            self.onViewTap(self.view)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag: Int = textField.tag + 1
        
        //println("nextTag: \(nextTag)")
        
        if let nextResponder: UITextField = textField.superview?.superview?.viewWithTag(nextTag) as? UITextField{
            nextResponder.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            self.onViewTap(self.view)
        }
        
        if textField.tag == 10{
            self.signupButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }
        
        return false
    }
    
    // MARK: - ContainerView tap handle
    
    func onViewTap(sender: UIView){
        for input in inputsArray{
            let inp: UITextField = input as! UITextField
            inp.resignFirstResponder()
        }
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y
        //self.scrollView.setContentOffset(CGPointZero, animated: true)
        if scrollViewHeight + scrollOffset > scrollContentSizeHeight{
            let y = scrollContentSizeHeight - scrollViewHeight
            let scrollPoint: CGPoint = CGPointMake(0, y)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    // MARK: - Signup Button Tap
    @IBAction func onSignupButtonTap(sender: UIButton) {
        self.onViewTap(self.scrollView)
        
        if firstnameField.text!.isEmpty{
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter your first name.", buttonNames: ["Okay"], completion: nil)
        }else if lastnameField.text!.isEmpty{
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter your last name.", buttonNames: ["Okay"], completion: nil)
        }else if usernameField.text!.isEmpty || !FormValidation.isValidEmail(usernameField.text!){
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter valid email.", buttonNames: ["Okay"], completion: nil)
        }else if passwordField.text!.isEmpty{
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter password.", buttonNames: ["Okay"], completion: nil)
        }else if addressField.text!.isEmpty{
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter valid address.", buttonNames: ["Okay"], completion: nil)
        }else if cityField.text!.isEmpty{
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter your city.", buttonNames: ["Okay"], completion: nil)
        }else if stateField.text!.isEmpty{
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter your state.", buttonNames: ["Okay"], completion: nil)
        }else if zipField.text!.isEmpty || !FormValidation.isValidZip(zipField.text!){
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter zip.", buttonNames: ["Okay"], completion: nil)
        }
        else if phoneField.text!.isEmpty || !FormValidation.isValidPhone(phoneField.text!){
            AlertManager.showAlert(self, title: "Warning!", message: "Please enter your phone number.", buttonNames: ["Okay"], completion: nil)
        }else{
            // activity indicator
            let activityIndicator = UICustomActivityView()
            activityIndicator.showActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
            
            let loginData: NSDictionary = ["User": ["firstname": firstnameField.text!, "lastname": lastnameField.text!, "email": usernameField.text!, "password": passwordField.text!, "ajax" : true, "address1": addressField.text!, "address2": addressField2.text!, "city": cityField.text!, "state": stateField.text!, "zip": zipField.text!, "phone": phoneField.text!]]
            
            //println(loginData)
            
            DataManager.postDataAsyncWithCallback("users/register", data: loginData, json: true, completion: { (data, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    activityIndicator.hideActivityIndicator()
                    if error == nil && data != nil{
                        let signupData = JSON(data: data!)
                        print(signupData)
                        
                        if signupData["success"] == true && signupData["signup"] == true{
                            let message = "Signup was successful."
                            AlertManager.showAlert(self, title: "Success", message: message, buttonNames: nil, completion: { (index) -> Void in
                                self.navigationController?.popViewControllerAnimated(true)
                            })
                        }else{
                            let message = signupData["message"]
                            AlertManager.showAlert(self, title: "Error", message: "\(message)", buttonNames: nil)
                        }
                        
                    }else if let posterror = error{
                        AlertManager.showAlert(self, title: "Error", message: "Error code: \(posterror.code). \(posterror.localizedDescription)", buttonNames: nil)
                    }
                    print(data)
                    print(error)
                })
            })
        }
    }
    
    // MARK: - Cancel Button Tap
    @IBAction func cancelButtoTap(sender: UIButton) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    // MARK: - Supported Orientation
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        /*let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS8 = iosVersion >= 8
        let iOS7 = iosVersion >= 7 && iosVersion < 8
        if iOS7{
            return Int(UIInterfaceOrientationMask.Portrait.rawValue)
        }
        return Int(UIInterfaceOrientationMask.All.rawValue)*/
        print(1)
        return UIInterfaceOrientationMask.Portrait
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
