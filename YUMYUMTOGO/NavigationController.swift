//
//  NavigationController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/14/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreData

// Global Variables to store informations
var isLoggedIn: Bool = false
var currentCartRestaurantId: String?

class NavigationController: UINavigationController, SideBarDelegate, RestaurantViewControllerDelegate, LoginViewControllerDelegate, CartTableViewControllerDelegate, InvoicesTableViewControllerDelegate {
    
    var sideBar: SideBar!
    let loggedInSideBarItems: NSMutableArray = [["title": "Restaurants", "icon": "icon-nav-list"], ["title": "Cart", "icon": "icon-nav-cart"], ["title": "Invoices", "icon": "icon-nav-invoice"], ["title": "Logout", "icon": "icon-nav-logout"]]
    let sideBarItems: NSMutableArray = [["title": "Restaurants", "icon": "icon-nav-list"], ["title": "Login", "icon": "icon-login"]]
    
    var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    var initialViewIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBarHidden = true
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 250 * 1000000), dispatch_get_main_queue()){
            self.sideBar = SideBar(sourceView: self.view.window!, menuItems: self.sideBarItems)
            self.sideBar.delegate = self
            self.sideBar.shouldDeselectSelectedRow = true
            self.sideBar.shouldHandleSwipe = true
            self.sideBar.navIcon = "iconMenu"
        }
        
        //self.clearCart()
        currentCartRestaurantId = self.getCartRestaurantID()
        print("currentCartRestaurantId: \(currentCartRestaurantId)")
        
        //prefs.setObject(nil, forKey: "username")
        
        if let username: String = prefs.objectForKey("username") as? String{
            if username != "" || !username.isEmpty{
                if let password: String = prefs.objectForKey("password") as? String{
                    if !password.isEmpty || password != ""{
                        let data: NSDictionary = ["User": ["email": username, "password": password]]
                        self.autoBackgroundLogin(data)
                    }else{
                        goLoginView()
                    }
                }else{
                    goLoginView()
                }
            }else{
                goLoginView()
            }
        }else{
            goLoginView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - registerPushNotification
    
    func registerPushNotification(){
        if #available(iOS 8.0, *) {
            let notificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
            UIApplication.sharedApplication().registerForRemoteNotifications()
        } else {
            UIApplication.sharedApplication().registerForRemoteNotificationTypes([UIRemoteNotificationType.Alert, UIRemoteNotificationType.Badge, UIRemoteNotificationType.Sound])
        }
    }
    
    // MARK: - Navigate Loginview

    func goLoginView(){
        prefs.setObject(nil, forKey: "username")
        prefs.setObject(nil, forKey: "password")
        prefs.setObject(nil, forKey: "userID")
        prefs.synchronize()
        let vc: LoginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("loginViewController") as! LoginViewController
        vc.delegate = self
        //let vc: MenusListViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MenusListViewController") as MenusListViewController
        //let vc: AddonsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddonsViewController") as AddonsViewController
        self.setViewControllers([vc], animated: false)
    }
    
    func autoLogin(data: AnyObject){
        let activityIndicator = UICustomActivityView()
        activityIndicator.showActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        
        let loginData: NSDictionary = data as! NSDictionary
        
        //println(loginData)
        
        DataManager.postDataAsyncWithCallback("users/login", data: loginData, json: true, completion: { (data, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                activityIndicator.hideActivityIndicator()
                if error == nil && data != nil{
                    let response = JSON(data: data!)
                    print(response)
                    if response["success"] == true && response["login"] == true{
                        self.registerPushNotification()
                        isLoggedIn = true
                        if let olduser: String = self.prefs.objectForKey("oldusername") as? String{
                            if let newUserData: NSDictionary = loginData.objectForKey("User") as? NSDictionary{
                                if let newUser: String = newUserData.objectForKey("email") as? String{
                                    if olduser != newUser{
                                        self.clearCart()
                                    }
                                }
                            }
                        }
                        self.sideBar.menuItems = self.loggedInSideBarItems
                        if self.initialViewIndex != nil{
                            self.sideBarDidSelectRowAtIndex(2, dict: self.sideBar.menuItems!.objectAtIndex(2) as! NSDictionary)
                        }else{
                            let vc: RestaurantViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                            vc.delegate = self
                            self.setViewControllers([vc], animated: false)
                        }
                    }else{
                        self.goLoginView()
                    }
                    
                }else if let posterror = error{
                    self.goLoginView()
                }
                //println(JSON(data: data!))
                //println(error)
            })
        })
    }
    
    func autoBackgroundLogin(data: AnyObject){
        let loginData: NSDictionary = data as! NSDictionary
        //println(loginData)
        
        DataManager.postDataAsyncWithCallback("users/login", data: loginData, json: true, completion: { (data, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if error == nil && data != nil{
                    let response = JSON(data: data!)
                    print(response)
                    if response["success"] == true && response["login"] == true{
                        self.registerPushNotification()
                        isLoggedIn = true
                        print("Auto login success")
                        if let olduser: String = self.prefs.objectForKey("oldusername") as? String{
                            if let newUserData: NSDictionary = loginData.objectForKey("User") as? NSDictionary{
                                if let newUser: String = newUserData.objectForKey("email") as? String{
                                    if olduser != newUser{
                                        self.clearCart()
                                    }
                                }
                            }
                        }
                        self.sideBar.menuItems = self.loggedInSideBarItems
                        if self.initialViewIndex != nil{
                            self.sideBarDidSelectRowAtIndex(2, dict: self.sideBar.menuItems!.objectAtIndex(2) as! NSDictionary)
                        }
                        //let vc: CartTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CartTableViewController") as CartTableViewController
                        //self.setViewControllers([vc], animated: false)
                    }else{
                        print("Auto login error")
                    }
                    
                }else if let posterror = error{
                    print("Auto login posterror")
                    print(posterror.localizedDescription)
                }
                //println(JSON(data: data!))
                //println(error)
            })
        })
        //*
        let vc: RestaurantViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
        vc.delegate = self
        self.setViewControllers([vc], animated: false)//*/
    }
    
    // MARK: - Orientation
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if let lastOrientation = self.viewControllers.last?.supportedInterfaceOrientations(){
            return self.viewControllers.last!.supportedInterfaceOrientations()
        }
        return UIInterfaceOrientationMask.All
    }
    
    override func shouldAutorotate() -> Bool {
        if self.viewControllers.last?.shouldAutorotate() != nil{
            return self.viewControllers.last!.shouldAutorotate()
        }
        return true
    }
    
    // MARK: - StatusBar Style
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        if let preferredStyle = self.viewControllers.last?.preferredStatusBarStyle(){
            return preferredStyle
        }
        return UIStatusBarStyle.Default
    }
    
    // MARK: - Sidebar delegate
    
    func sideBarDidSelectRowAtIndex(index: Int, dict: NSDictionary) {
        sideBar.showSideBar(false)
        //let dict: NSDictionary = sideBarItems.objectAtIndex(index) as NSDictionary
        //println(dict)
        if (dict.objectForKey("title") as! NSString).lowercaseString == "logout" || (dict.objectForKey("title") as! NSString).lowercaseString == "login"{
            (UIApplication.sharedApplication().delegate as! AppDelegate).unregisterDevice()
            isLoggedIn = false
            goLoginView()
        }else if (dict.objectForKey("title") as! NSString).lowercaseString == "cart"{
            let vc: CartTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CartTableViewController") as! CartTableViewController
            vc.delegate = self
            self.setViewControllers([vc], animated: false)
        }else if (dict.objectForKey("title") as! NSString).lowercaseString == "invoices"{
            let vc: InvoicesTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("InvoicesTableViewController") as! InvoicesTableViewController
            vc.delegate = self
            self.setViewControllers([vc], animated: false)
        }else if (dict.objectForKey("title") as! NSString).lowercaseString == "restaurants"{
            let vc: RestaurantViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
            vc.delegate = self
            self.setViewControllers([vc], animated: false)
        }
    }
    
    func updateSidebarMenu(){
        if isLoggedIn{
            self.sideBar.menuItems = self.loggedInSideBarItems
        }else{
            self.sideBar.menuItems = self.sideBarItems
        }
    }
    
    // MARK: - RestaurantViewControllerDelegate
    func onMenuButtonTap() {
        sideBar.showSideBar(true)
    }
    
    // MARK: - getCartRestaurantID
    func getCartRestaurantID() -> String?{
        let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("database")
        let sorter: NSSortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        let results: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: [sorter], managedObjectContext: moc, limit: 1)
        //println("getCartRestaurantID results.count: \(results.count)")
        if results.count > 0{
            /*for result in results{
                let cart: Carts = result as Carts
                println("cartid: \(cart.id)")
            }*/
            let lastObject: Carts = results.lastObject as! Carts
            return lastObject.restaurant_id
        }
        
        return nil
    }
    
    // MARK: - Clear Local Cart Items
    func clearCart() {
        let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("database")
        let results: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc)
        for result in results{
            let cart: Carts = result as! Carts
            moc.deleteObject(cart)
        }
        var error: NSError? = nil
        do {
            try moc.save()
        } catch var error1 as NSError {
            error = error1
        }
        if error != nil{
            print(error?.localizedDescription)
        }else{
            print("All record deleted")
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
