//
//  MenusListViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 2/2/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

protocol MenusListViewControllerDelegate{
    func didSelectMenuAtIndex(index: Int, rowdata: NSDictionary?, restaurantdata: NSDictionary?)
}

class MenusListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, LoginViewControllerDelegate {
    
    var delegate: MenusListViewControllerDelegate?

    @IBOutlet var menusTable: UITableView!
    @IBOutlet var banner: UIImageView!
    @IBOutlet var shortDescription: UITextView!
    @IBOutlet var restaurantType: CellTypeLabel!
    @IBOutlet var restaurantCuisine: CellTypeLabel!
    @IBOutlet var menusCollectionView: UICollectionView!
    @IBOutlet var noItemLabel: UILabel!
    
    var restaurantData: NSDictionary?
    
    var tableData: NSMutableArray = NSMutableArray()
    
    var selectedIndexPath: NSIndexPath? = nil
    var selectedThumIndexPath: NSIndexPath? = nil
    
    var activityIndicatorParentView: AnyObject!
    var isUploading: Bool = false
    
    var listBarButton: UIBarButtonItem!
    var gridBarButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let indicatorSuperview = self.parentViewController?.view.window != nil ? self.parentViewController?.view.window! : (self.view.window != nil ? self.view.window! : self.view)
        if activityIndicatorParentView == nil{
            activityIndicatorParentView = indicatorSuperview
        }
        
        //self.view.backgroundColor = UIColor(rgba: "ff3b30")
        
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.tintColor = UIColor(rgba: "ffffff")
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "ff3b30")
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(20)]
        self.navigationItem.hidesBackButton = false
        // set back button for child view
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Menus", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        
        listBarButton = UIBarButtonItem()
        listBarButton.image = UIImage(named: "icon-list")
        listBarButton.action = Selector("toogleMenus:")
        listBarButton.target = self
        gridBarButton = UIBarButtonItem()
        gridBarButton.image = UIImage(named: "icon-grid")
        gridBarButton.action = Selector("toogleMenus:")
        gridBarButton.target = self
        self.navigationItem.rightBarButtonItem = listBarButton
        

        self.menusTable.contentInset = UIEdgeInsetsMake(150, 0, 0, 0)
        self.menusTable.backgroundColor = UIColor.clearColor()
        self.menusTable.showsVerticalScrollIndicator = false
        self.menusTable.dataSource = self
        self.menusTable.delegate = self
        
        self.menusTable.rowHeight = 112
        self.menusTable.registerNib(UINib(nibName: "MenusTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.menusTable.separatorInset = UIEdgeInsetsZero
        
        self.menusCollectionView.contentInset = UIEdgeInsetsMake(150, 0, 0, 0)
        self.menusCollectionView.backgroundColor = UIColor.clearColor()
        self.menusCollectionView.showsVerticalScrollIndicator = false
        self.menusCollectionView.dataSource = self
        self.menusCollectionView.delegate = self
        self.menusCollectionView.registerNib(UINib(nibName: "MenusCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        
        let footerView: UIView = UIView(frame: CGRectMake(0, 0, 100, 1))
        footerView.backgroundColor = UIColor.clearColor()
        self.menusTable.tableFooterView = footerView
        
        if restaurantData != nil{
            //println(restaurantData)
            self.navigationItem.title = restaurantData?.objectForKey("name") as! String!
            if let shorttext: String = restaurantData?.objectForKey("description") as? String{
                self.menusTable.contentInset = UIEdgeInsetsMake(200, 0, 0, 0)
                self.menusCollectionView.contentInset = UIEdgeInsetsMake(200, 0, 0, 0)
                //self.shortDescription.text = "Lorem ipsum. Restaurant short description goes here. How's it looking?"
                self.shortDescription.text = shorttext
                self.shortDescription.textColor = UIColor.blackColor()
                self.shortDescription.textContainer.lineFragmentPadding = 0
                self.shortDescription.textContainerInset = UIEdgeInsetsZero
            }
            if let restype: String = restaurantData?.objectForKey("type") as? String{
                if !restype.isEmpty || restype != ""{
                    self.restaurantType.text = "\(restype)\u{2007}\u{2007}"
                    self.restaurantType.hidden = false
                    self.restaurantType.layer.borderColor = UIColor.whiteColor().CGColor
                    self.restaurantType.layer.borderWidth = 1
                    self.restaurantType.layer.cornerRadius = 8
                    self.restaurantType.clipsToBounds = true
                }
            }
            
            if let cuisine: String = restaurantData?.objectForKey("cuisine") as? String{
                if !cuisine.isEmpty || cuisine != ""{
                    self.restaurantCuisine.text = "\(cuisine)\u{2007}\u{2007}"
                    self.restaurantCuisine.hidden = false
                    self.restaurantCuisine.layer.borderColor = UIColor.whiteColor().CGColor
                    self.restaurantCuisine.layer.borderWidth = 1
                    self.restaurantCuisine.layer.cornerRadius = 8
                    self.restaurantCuisine.clipsToBounds = true
                }
            }
            
            let queue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
            dispatch_async(queue){
                let url: String = DataManager.domain().root + "img/restaurants/banner/" + (self.restaurantData?.objectForKey("banner") as! String)
                //println(url)
                do {
                    let imageData: NSData = try NSData(contentsOfURL: NSURL(string: url)!, options: NSDataReadingOptions())
                    dispatch_async(dispatch_get_main_queue()){
                        self.banner.image = UIImage(data: imageData)
                    }
                } catch {
                    fatalError()
                }
            }
            
            self.loadMenusData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return tableData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: MenusTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! MenusTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        //cell.contentView.backgroundColor = UIColor(rgba: "fb6c64")
        
        let cellData: NSDictionary = self.tableData.objectAtIndex(indexPath.row) as! NSDictionary
        
        if AlertManager.isIOS8(){
            if #available(iOS 8.0, *) {
                cell.layoutMargins = UIEdgeInsetsZero
                cell.preservesSuperviewLayoutMargins = false
            } else {
                // Fallback on earlier versions
            }
        }
        
        if indexPath.row % 2 == 0{
            cell.price.backgroundColor = UIColor(rgba: "ec8025")
        }else{
            cell.price.backgroundColor = UIColor(rgba: "50ADB4")
        }
        
        cell.title.text = cellData.objectForKey("name") as? String
        let formatter: NSNumberFormatter = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
        formatter.maximumFractionDigits = 2
        
        if let price: NSString = cellData.objectForKey("price") as? NSString{
            let nPrice: Double = price.doubleValue
            cell.price.text = "$\(formatter.stringFromNumber(nPrice)!)\u{2007}\u{2007}"
        }else{
            cell.price.hidden = true
        }
        cell.shortDescription.text = cellData.objectForKey("description") as? String
        /*
        let queue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
        dispatch_async(queue){
            let url: String = DataManager.domain().root + "img/menus/thumb/" + (cellData.objectForKey("thumb") as String)
            //println(url)
            var error: NSError?
            if let imageData: NSData = NSData(contentsOfURL: NSURL(string: url)!, options: NSDataReadingOptions.allZeros, error: &error){
                dispatch_async(dispatch_get_main_queue()){
                    cell.thumbImage.image = UIImage(data: imageData)
                }
            }
        }
        */
        let url: String = DataManager.domain().root + "img/menus/thumb/" + (cellData.objectForKey("thumb") as! String)
        cell.thumbImage.sd_setImageWithURL(NSURL(string: url)!, placeholderImage: UIImage(named: "take-photo"))
        
        //cell.rightButtons = [MGSwipeButton(title: "Remove \nFrom Cart", backgroundColor: UIColor.redColor())]
        //cell.rightSwipeSettings.transition = MGSwipeTransition.TransitionClipCenter
        
        //cell.leftButtons = [MGSwipeButton(title: "Add \nTo Cart", icon: UIImage(named: "addtocart"), backgroundColor: UIColor(rgba: "5c8c27"))]
        //cell.leftSwipeSettings.transition = MGSwipeTransition.TransitionClipCenter
        //cell.delegate = self
        
        return cell
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if selectedIndexPath != nil{
            let cell: MenusTableViewCell? = tableView.cellForRowAtIndexPath(selectedIndexPath!) as? MenusTableViewCell
            cell?.contentView.layer.borderColor = UIColor.clearColor().CGColor
            cell?.contentView.layer.borderWidth = 0
        }
        let cell: MenusTableViewCell = tableView.cellForRowAtIndexPath(indexPath) as! MenusTableViewCell
        cell.contentView.layer.borderColor = UIColor.whiteColor().CGColor
        cell.contentView.layer.borderWidth = 1
        selectedIndexPath = indexPath
        
        return true
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        if selectedIndexPath != nil{
            let cell: MenusTableViewCell = tableView.cellForRowAtIndexPath(selectedIndexPath!) as! MenusTableViewCell
            cell.contentView.layer.borderColor = UIColor.clearColor().CGColor
            cell.contentView.layer.borderWidth = 0
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = tableData.objectAtIndex(indexPath.row) as! NSDictionary
        delegate?.didSelectMenuAtIndex(indexPath.row, rowdata: cellData, restaurantdata: restaurantData)
    }
    
    // MARK: - MGSwipeCellDelegete
    func swipeTableCell(cell: MGSwipeTableCell!, canSwipe direction: MGSwipeDirection) -> Bool {
        return true
    }
    
    func swipeTableCell(cell: MGSwipeTableCell!, tappedButtonAtIndex index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        let cell: MenusTableViewCell = cell as! MenusTableViewCell
        if direction == MGSwipeDirection.LeftToRight{
            AlertManager.showAlert(self, title: "Confirmation", message: "Do you want to add \(cell.title.text!) to cart. Add again to increase quantity. You can also always change quantity in cart.", buttonNames: ["Yes", "No"], completion: nil)
        }else{
            AlertManager.showAlert(self, title: "Confirmation", message: "You are about to remove \(cell.title.text!) from cart. Remove again to decrease quantity. You can also always change quantity in cart.", buttonNames: ["Yes", "No"], completion: nil)
        }
        return false
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tableData.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: MenusCollectionViewCell = menusCollectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! MenusCollectionViewCell
        
        let cellData: NSDictionary = self.tableData.objectAtIndex(indexPath.row) as! NSDictionary
        
        cell.title.text = cellData.objectForKey("name") as? String
        let formatter: NSNumberFormatter = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
        formatter.maximumFractionDigits = 2
        
        if let price: NSString = cellData.objectForKey("price") as? NSString{
            let nPrice: Double = price.doubleValue
            cell.price.text = "$\(formatter.stringFromNumber(nPrice)!)\u{2007}\u{2007}"
        }else{
            cell.price.hidden = true
        }
        /*
        let queue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
        dispatch_async(queue){
            cell.thumbImage.userInteractionEnabled = false
            let url: String = DataManager.domain().root + "img/menus/thumb/" + (cellData.objectForKey("thumb") as String)
            //println(url)
            var error: NSError?
            if let imageData: NSData = NSData(contentsOfURL: NSURL(string: url)!, options: NSDataReadingOptions.allZeros, error: &error){
                dispatch_async(dispatch_get_main_queue()){
                    cell.cameraButton.hidden = false
                    cell.cameraButton.addTarget(self, action: "onCellCameraTap:", forControlEvents: UIControlEvents.TouchUpInside)
                    cell.thumbImage.image = UIImage(data: imageData)
                }
            }
            if error != nil{
                dispatch_async(dispatch_get_main_queue()){
                    cell.cameraButton.hidden = true
                    cell.thumbImage.image = UIImage(named: "take-photo")
                    cell.thumbImage.userInteractionEnabled = true
                    let thumImageTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onThumbImageTap:")
                    thumImageTap.numberOfTapsRequired = 1
                    cell.thumbImage.addGestureRecognizer(thumImageTap)
                }
            }
        }
        */
        let url: String = DataManager.domain().root + "img/menus/thumb/" + (cellData.objectForKey("thumb") as! String)
        cell.thumbImage.sd_setImageWithURL(NSURL(string: url)!, placeholderImage: UIImage(named: "take-photo"))
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("didSelectItemAtIndexPath: \(indexPath.row)")
        //selectedThumIndexPath = nil
        let cellData: NSDictionary = tableData.objectAtIndex(indexPath.row) as! NSDictionary
        delegate?.didSelectMenuAtIndex(indexPath.row, rowdata: cellData, restaurantdata: restaurantData)
    }
    
    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell: MenusCollectionViewCell = menusCollectionView.cellForItemAtIndexPath(indexPath) as! MenusCollectionViewCell
        if cell.cameraButton.hidden == false{
            cell.cameraButton.alpha = 1.0
        }
    }
    
    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell: MenusCollectionViewCell = menusCollectionView.cellForItemAtIndexPath(indexPath) as! MenusCollectionViewCell
        if cell.cameraButton.hidden == false{
            cell.cameraButton.alpha = 0.5
        }
    }
    
    // MARK: - LoginViewControllerDelegate
    func didLogggeIn(success: Bool) {
        if success == true{
            if selectedThumIndexPath != nil{
                if let cell: MenusCollectionViewCell = menusCollectionView.cellForItemAtIndexPath(selectedThumIndexPath!) as? MenusCollectionViewCell{
                    self.showImagePickerViewController(cell)
                }
            }
        }else{
            selectedThumIndexPath = nil
        }
    }
    
    // MARK: - onThumbImageTap
    
    func onThumbImageTap(gesture: UITapGestureRecognizer){
        if isUploading == true{
            return
        }
        if isLoggedIn == false{
            if let cell: MenusCollectionViewCell = gesture.view?.superview?.superview as? MenusCollectionViewCell{
                selectedThumIndexPath = self.menusCollectionView.indexPathForCell(cell)
            }
            AlertManager.showAlert(self, title: "Warning!", message: "Log in is required for this action", buttonNames: ["Cancel", "Login"], completion: { (index) -> Void in
                if index == 1{
                    let loginVC: LoginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("loginViewController") as! LoginViewController
                    loginVC.delegate = self
                    loginVC.shouldHideOnLogin = true
                    self.presentViewController(loginVC, animated: true, completion: nil)
                }else{
                    self.selectedThumIndexPath = nil
                }
            })
            return
        }
        if let cell: MenusCollectionViewCell = gesture.view?.superview?.superview as? MenusCollectionViewCell{
            self.showImagePickerViewController(cell)
        }
    }
    
    // MARK: - onCellCameraTap
    
    func onCellCameraTap(sender: UIButton){
        if isUploading == true{
            return
        }
        if isLoggedIn == false{
            if let cell: MenusCollectionViewCell = sender.superview?.superview as? MenusCollectionViewCell{
                selectedThumIndexPath = self.menusCollectionView.indexPathForCell(cell)
            }
            AlertManager.showAlert(self, title: "Warning!", message: "Log in is required for this action", buttonNames: ["Cancel", "Login"], completion: { (index) -> Void in
                if index == 1{
                    let loginVC: LoginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("loginViewController") as! LoginViewController
                    loginVC.delegate = self
                    loginVC.shouldHideOnLogin = true
                    self.presentViewController(loginVC, animated: true, completion: nil)
                }else{
                    self.selectedThumIndexPath = nil
                }
            })
            return
        }
        if let cell: MenusCollectionViewCell = sender.superview?.superview as? MenusCollectionViewCell{
            self.showImagePickerViewController(cell)
        }
    }
    
    func showImagePickerViewController(cell: MenusCollectionViewCell){
        selectedThumIndexPath = self.menusCollectionView.indexPathForCell(cell)
        print(selectedThumIndexPath)
        let imagePicker: UIImagePickerController = UIImagePickerController()
        if UIDevice.currentDevice().model == "iPhone Simulator"{
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }else{
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        imagePicker.delegate = self
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let imageInfo = info as NSDictionary
        
        let pickedImage: UIImage = imageInfo.objectForKey(UIImagePickerControllerOriginalImage) as! UIImage
        
        //scale down image
        let scaledImage = self.scaleImageWith(pickedImage, newSize: CGSizeMake(160, 120), scale: CGFloat(pickedImage.size.width / pickedImage.size.height))
        
        let imageData = UIImagePNGRepresentation(scaledImage)
        
        if selectedThumIndexPath != nil{
            let cell: MenusCollectionViewCell = menusCollectionView.cellForItemAtIndexPath(selectedThumIndexPath!) as! MenusCollectionViewCell
            
            //println(cell)
            
            let cellData: NSMutableDictionary = NSMutableDictionary(dictionary: self.tableData.objectAtIndex(selectedThumIndexPath!.row) as! NSDictionary)
            let menuID: String = cellData.objectForKey("id") as! String
            
            //cell.activityIndicator.hidden = false
            cell.activityIndicator.startAnimating()
            
            let base64String: String = imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
            let postData: NSDictionary = ["Menu": ["thumb": base64String]]
            print("menus/upload/\(menuID)")
            //println(postData)
            isUploading = true
            DataManager.postDataAsyncWithCallback("menus/upload/\(menuID)", data: postData, json: true, completion: { (data, error) -> Void in
                dispatch_async(dispatch_get_main_queue()){
                    self.isUploading = false
                    cell.activityIndicator.stopAnimating()
                    //println(data)
                    //println(error)
                    if error == nil && data != nil{
                        if let response: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSDictionary{
                            //println(response)
                            if response.objectForKey("success") as! Bool == true{
                                cell.cameraButton.hidden = false
                                cell.thumbImage.image = UIImage(data: imageData!)
                                if let thumb: String = response.objectForKey("thumb") as? String{
                                    cellData.setValue(thumb, forKey: "thumb")
                                    if self.selectedThumIndexPath != nil{
                                        self.tableData.replaceObjectAtIndex(self.selectedThumIndexPath!.row, withObject: cellData)
                                    }
                                }
                                if let recognizers = cell.thumbImage.gestureRecognizers {
                                    for recognizer in recognizers {
                                        cell.thumbImage.removeGestureRecognizer(recognizer )
                                    }
                                }
                            }
                        }
                        /*if let response: NSString = NSString(data: data!, encoding: NSUTF8StringEncoding){
                            println(response)
                        }*/
                    }else if error != nil{
                        AlertManager.showAlert(self, title: "Error!", message: error!.localizedDescription, buttonNames: nil, completion: nil)
                    }
                }
                
            })
        }
        
        picker.dismissViewControllerAnimated(true, completion:nil)
    }
    
    func scaleImageWith( image: UIImage, newSize: CGSize, scale: CGFloat? = 0.0) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, scale!)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

    
    // MARK: - UIStatusBarStyle
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    // MARK: - Load Menus Data
    func loadMenusData(){
        let activityIndicator = UICustomActivityView()
        activityIndicator.showActivityIndicator(activityIndicatorParentView, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        let url: String = "menus/lists?restaurant_id=" + (restaurantData?.objectForKey("id") as! String!)
        //println(url)
        DataManager.loadDataAsyncWithCallback(url, completion: { (data, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                activityIndicator.hideActivityIndicator()
                //println(error)
                //println(data)
                if error == nil && data != nil{
                    //println(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    if let jsonData: NSArray = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSArray{
                        if jsonData.count > 0{
                            self.tableData.removeAllObjects()
                            for value in jsonData{
                                self.tableData.addObject(value as! NSDictionary)
                            }
                            if self.tableData.count > 0{
                                self.noItemLabel.hidden = true
                                if self.menusTable.hidden == false{
                                    self.menusTable.reloadData()
                                }
                                if self.menusCollectionView.hidden == false{
                                    self.menusCollectionView.reloadData()
                                }
                            }else{
                                self.noItemLabel.hidden = false
                                self.view.bringSubviewToFront(self.noItemLabel)
                            }
                        }
                    }else{
                        self.noItemLabel.hidden = false
                        self.view.bringSubviewToFront(self.noItemLabel)
                    }
                }else{
                    self.noItemLabel.hidden = false
                    self.view.bringSubviewToFront(self.noItemLabel)
                }
            })
        })
    }
    
    // MARK: - toogleMenus
    func toogleMenus(sender: AnyObject){
        if self.tableData.count > 0{
            self.noItemLabel.hidden = true
            if self.menusTable.hidden == false{
                self.navigationItem.rightBarButtonItem = listBarButton
                self.menusTable.hidden = true
                self.menusCollectionView.hidden = false
                self.menusCollectionView.reloadData()
            }else if self.menusCollectionView.hidden == false{
                self.navigationItem.rightBarButtonItem = gridBarButton
                self.menusCollectionView.hidden = true
                self.menusTable.hidden = false
                self.menusTable.reloadData()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
