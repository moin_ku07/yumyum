//
//  FormValidation.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/17/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

class FormValidation: NSObject {
    class func isValidEmail(candidate:String) -> Bool {
        //println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(candidate)
        return result
    }
    class func isValidZip(candidate:String) -> Bool {
        let zipRegEx = "^(\\d{5}(-\\d{4})?|[a-z]\\d[a-z][- ]*\\d[a-z]\\d)$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", zipRegEx)
        let result = emailTest.evaluateWithObject(candidate)
        return result
    }
    class func isValidCity(candidate:String) -> Bool {
        let zipRegEx = "^[A-Za-z]+(\\s[A-Za-z]+)?$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", zipRegEx)
        let result = emailTest.evaluateWithObject(candidate)
        return result
    }
    class func isValidPhone(candidate:String) -> Bool {
        let regex = "((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", regex)
        let result = emailTest.evaluateWithObject(candidate)
        return result
    }
}
