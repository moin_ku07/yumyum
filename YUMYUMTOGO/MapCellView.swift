//
//  MapCellView.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/31/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

class MapCellView: UIView {
    var thumbView: UIImageView!
    var thumb: String?{
        didSet{
            /*let queue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
            dispatch_async(queue){
                let url: NSURL = NSURL(string: self.thumb!)!
                if let imageData: NSData = NSData(contentsOfURL: url){
                    dispatch_async(dispatch_get_main_queue()){
                        self.thumbView.image = UIImage(data: imageData)
                    }
                }
            }*/
            let url: NSURL = NSURL(string: self.thumb!)!
            self.thumbView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "take-photo"))
        }
    }
    
    var title: String?{
        didSet{
            self.titleLabel.text = self.title
        }
    }
    var titleLabel: UILabel!
    
    var cuisine: String?{
        didSet{
            self.cuisineLabel.text = self.cuisine
        }
    }
    var cuisineLabel: UILabel!
    
    var distance: String?{
        didSet{
            self.distanceLabel.text = self.distance
        }
    }
    var distanceLabel: UILabel!
    
    var type: String?{
        didSet{
            if self.type! != ""{
                self.typeLabel.text = "\(self.type!)\u{2007}\u{2007}"
                self.typeLabel.hidden = false
            }
        }
    }
    var typeLabel: CellTypeLabel!
    
    var cellData: NSDictionary?
    var cellIndex: Int?
    
    convenience init() {
        self.init(frame: CGRectZero)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView(){
        self.backgroundColor = UIColor(red: 255/255, green: 59/255, blue: 48/255, alpha: 0.9)
        self.clipsToBounds = true
        
        thumbView = UIImageView(frame: CGRectMake(0, 0, 150, 112))
        thumbView.contentMode = UIViewContentMode.ScaleAspectFill
        self.addSubview(thumbView)
        
        let maskLayer: CALayer = CALayer()
        maskLayer.frame = thumbView.bounds
        maskLayer.shadowRadius = 10
        //maskLayer.shadowPath = CGPathCreateWithRoundedRect(CGRectInset(imageview.bounds, 5, 5), 10, 10, nil)
        //println(thumbImage.frame.size.width)
        maskLayer.shadowPath = CGPathCreateWithRect(CGRectMake(-15, -5, thumbView.frame.size.width - 10, thumbView.frame.size.height + 30), nil)
        //maskLayer.shadowPath = CGPathCreateWithRect(CGRectInset(imageview.bounds, 5, 5), nil)
        maskLayer.shadowOpacity = 1
        maskLayer.shadowOffset = CGSizeZero
        maskLayer.shadowColor = UIColor.whiteColor().CGColor
        
        thumbView.layer.mask = maskLayer
        
        titleLabel = UILabel()
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont.boldSystemFontOfSize(15)
        titleLabel.textAlignment = .Right
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)
        
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 8))
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 8))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: titleLabel, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 8))
        
        cuisineLabel = UILabel()
        cuisineLabel.textColor = UIColor.whiteColor()
        cuisineLabel.font = UIFont.systemFontOfSize(15)
        cuisineLabel.textAlignment = .Right
        cuisineLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(cuisineLabel)
        
        self.addConstraint(NSLayoutConstraint(item: cuisineLabel, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: cuisineLabel, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 8))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: cuisineLabel, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 8))
        
        distanceLabel = UILabel()
        distanceLabel.textColor = UIColor.whiteColor()
        distanceLabel.font = UIFont.systemFontOfSize(12)
        distanceLabel.textAlignment = .Right
        distanceLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(distanceLabel)
        
        self.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: distanceLabel, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 4))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: distanceLabel, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 8))
        self.addConstraint(NSLayoutConstraint(item: distanceLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 120))
        
        typeLabel = CellTypeLabel()
        typeLabel.hidden = true
        typeLabel.backgroundColor = UIColor(rgba: "50ADB4")
        typeLabel.textColor = UIColor.whiteColor()
        typeLabel.font = UIFont.systemFontOfSize(12)
        typeLabel.textAlignment = .Center
        typeLabel.layer.borderColor = UIColor.whiteColor().CGColor
        typeLabel.layer.borderWidth = 1
        typeLabel.layer.cornerRadius = 8
        typeLabel.clipsToBounds = true
        typeLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(typeLabel)
        
        self.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: typeLabel, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 4))
        self.addConstraint(NSLayoutConstraint(item: typeLabel, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 4))
        self.addConstraint(NSLayoutConstraint(item: typeLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.GreaterThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 10))
        self.addConstraint(NSLayoutConstraint(item: typeLabel, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 18))
        
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
