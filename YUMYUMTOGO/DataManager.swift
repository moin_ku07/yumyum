//
//  DataManager.swift
//  TopApps
//
//  Created by Dani Arnaout on 9/2/14.
//  Edited by Eric Cerney on 9/27/14.
//  Copyright (c) 2014 Ray Wenderlich All rights reserved.
//

import Foundation


class DataManager {
    
    struct domain {
        let url: String = "http://yumyum.durlov.com/api/"
        let root: String = "http://yumyum.durlov.com/"
    }
    
    class func postDataAsyncWithCallback(url: NSString, data: AnyObject, json: Bool? = true, completion: (data: NSData?, error: NSError?) -> Void){
        //let nsurl:NSURL = NSURL(string: url)!
        let nsurl:NSURL = NSURL(string: (self.domain().url + (url as String)))!
        print(nsurl)
        
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = "POST"
        
        if json == true{
            let postData: Dictionary = NSDictionary(dictionary: data as! [NSObject : AnyObject]) as Dictionary
            do {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(postData, options: NSJSONWritingOptions())
            } catch{
                request.HTTPBody = nil
            }
            //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
        }else{
            let postString: NSString = data as! NSString
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        }
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response: NSURLResponse?, urlData: NSData?, reponseError: NSError?) -> Void in
            if(urlData != nil ) {
                if let httpResponse = response as? NSHTTPURLResponse {
                    if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                        /*
                        var jsonParseError: NSError? = nil
                        let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                        println(object)
                        */
                        completion(data: urlData, error: nil)
                    }else{
                        let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                        completion(data: nil, error: statusError)
                    }
                }
            }else {
                if let error: NSError = reponseError {
                    completion(data: nil, error: error)
                }
            }
        }

    }
    
    class func postDataSyncWithCallback(url: NSString, data: AnyObject, json: Bool? = true, completion: (data: NSData?, error: NSError?) -> Void){
        //let nsurl:NSURL = NSURL(string: url)!
        let nsurl:NSURL = NSURL(string: (self.domain().url + (url as String)))!
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = "POST"
        
        if json == true{
            let postData: Dictionary = NSDictionary(dictionary: data as! [NSObject : AnyObject]) as Dictionary
            do {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(postData, options: NSJSONWritingOptions())
            } catch{
                request.HTTPBody = nil
            }
            //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
        }else{
            let postString: NSString = data as! NSString
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        }
        
        var reponseError: NSError?
        var response: NSURLResponse?
        
        var urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch{
            reponseError = error as? NSError
            urlData = nil
        }
        
        if(urlData != nil ) {
            if let httpResponse = response as? NSHTTPURLResponse {
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                    /*
                    var jsonParseError: NSError? = nil
                    let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                    println(object)
                    */
                    completion(data: urlData, error: nil)
                }else{
                    let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                }
            }
        }else {
            if let error = reponseError {
                completion(data: nil, error: error)
            }
        }
        
    }
    
    class func loadDataSyncWithCallback(url: NSString, completion: (data: NSData?, error: NSError?) -> Void){
        let nsurl:NSURL = NSURL(string: (self.domain().url + (url as String)))!
        
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = "GET"
        //request.HTTPBody = NSJSONSerialization.dataWithJSONObject(jsonData, options: nil, error: &err)
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        var reponseError: NSError?
        var response: NSURLResponse?
        
        var urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch{
            reponseError = error as? NSError
            urlData = nil
        }
        
        if(urlData != nil ) {
            if let httpResponse = response as? NSHTTPURLResponse {
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                    /*
                    var jsonParseError: NSError? = nil
                    let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                    println(object)
                    */
                    completion(data: urlData, error: nil)
                }else{
                    let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                }
            }
        }else {
            if let error = reponseError {
                completion(data: nil, error: error)
            }
        }
        
    }
    
    class func loadDataAsyncWithCallback(url: NSString, completion: (data: NSData?, error: NSError?) -> Void){
        let nsurl:NSURL = NSURL(string: (self.domain().url + (url as String)))!
        print(nsurl)
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = "GET"
        //request.HTTPBody = NSJSONSerialization.dataWithJSONObject(jsonData, options: nil, error: &err)
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response: NSURLResponse?, urlData: NSData?, reponseError: NSError?) -> Void in
            if(urlData != nil ) {
                if let httpResponse = response as? NSHTTPURLResponse {
                    if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                        /*
                        var jsonParseError: NSError? = nil
                        let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                        println(object)
                        */
                        completion(data: urlData, error: nil)
                    }else{
                        let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                        completion(data: nil, error: statusError)
                    }
                }
            }else {
                if let error = reponseError {
                    completion(data: nil, error: error)
                }
            }
        }
        
    }
    
}