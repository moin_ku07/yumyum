//
//  CellTypeLabel.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/29/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

class CellTypeLabel: UILabel {
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 5)
        //super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
        super.drawTextInRect(rect)
    }
    

}
