//
//  CartTableViewCell.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 2/25/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

protocol CartTableViewCellDelegate{
    func onQuantityChange(indexPath: NSIndexPath, oldValue: Int, newValue: Int, price: Float)
}

class CartTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    var delegate: CartTableViewCellDelegate?
    
    @IBOutlet var quantity: UITextField!
    @IBOutlet var title: UILabel!
    @IBOutlet var price: UILabel!
    
    var data: NSDictionary?{
        didSet{
            self.setupCell()
        }
    }
    
    var tableView: UITableView?{
        get{
            return self.getTableView()
        }
    }
    
    var originalTableVeiwContentOffset: CGPoint!
    var originalTableVeiwContentSize: CGSize!
    
    var selectedTextField: UITextField?
    var oldQuantity: Int?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //self.backgroundColor = UIColor.whiteColor()
        //self.contentView.backgroundColor = UIColor.whiteColor()
        
        self.title.text = ""
        self.quantity.text = ""
        self.price.text = ""
        
        quantity.backgroundColor = UIColor.clearColor()
        quantity.layer.borderColor = UIColor.grayColor().CGColor
        quantity.layer.borderWidth = 1
        quantity.delegate = self
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(){
        if data != nil{
            if let title: String = self.data!.objectForKey("menu_name") as? String{
                //println("title: \(title)")
                self.title.text = title
            }else if let title: String = self.data!.objectForKey("name") as? String{
                self.title.text = title
            }
            if let quantity: String = self.data!.objectForKey("quantity") as? String{
                //println("quantity: \(quantity)")
                self.quantity.text = quantity
            }
            
            if let price: String = self.data!.objectForKey("menu_price") as? String{
                //println(NSString(format: "$%@", price))
                self.price.text = NSString(format: "$%@", price) as String
            }else if let price: String = self.data!.objectForKey("price") as? String{
                self.price.text = NSString(format: "$%@", price) as String
            }
            /*
            if let addons: NSArray = data!.objectForKey("addons") as? NSArray{
                if addons.count > 0{
                    if AlertManager.isIOS8(){
                        self.layoutMargins = UIEdgeInsetsMake(0, 10, 0, 0)
                        self.preservesSuperviewLayoutMargins = true
                    }else{
                        self.separatorInset = UIEdgeInsetsMake(0, 10, 0, 0)
                    }
                }else{
                    if AlertManager.isIOS8(){
                        self.layoutMargins = UIEdgeInsetsZero
                        self.preservesSuperviewLayoutMargins = false
                    }else{
                        self.separatorInset = UIEdgeInsetsZero
                    }
                }
            }else{
                if AlertManager.isIOS8(){
                    self.layoutMargins = UIEdgeInsetsZero
                    self.preservesSuperviewLayoutMargins = false
                }else{
                    self.separatorInset = UIEdgeInsetsZero
                }
            }
            
            if let isAddon: Bool = data!.objectForKey("isAddon") as? Bool{
                if isAddon == true{
                    if AlertManager.isIOS8(){
                        self.layoutMargins = UIEdgeInsetsMake(0, 10, 0, 0)
                        self.preservesSuperviewLayoutMargins = true
                    }else{
                        self.separatorInset = UIEdgeInsetsMake(0, 10, 0, 0)
                    }
                }else{
                    if AlertManager.isIOS8(){
                        self.layoutMargins = UIEdgeInsetsZero
                        self.preservesSuperviewLayoutMargins = false
                    }else{
                        self.separatorInset = UIEdgeInsetsZero
                    }
                }
            }*/
        }
    }
    
    func getTableView() -> UITableView?{
        if self.superview?.isKindOfClass(UITableView.self) == true{
            return self.superview as? UITableView
        }else if self.superview?.superview?.isKindOfClass(UITableView.self) == true{
            return self.superview!.superview as? UITableView
        }
        return nil
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
         // Create a button bar for the number pad
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        
        // Setup the buttons to be put in the system.
        let item: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: Selector("onToolbarDoneTap") )
        let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let toolbarButtons = [flexSpace,item]
        
        //Put the buttons into the ToolBar and display the tool bar
        keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
        textField.inputAccessoryView = keyboardDoneButtonView
        
        self.selectedTextField = textField
        
        if let indexPath: NSIndexPath = self.tableView?.indexPathForCell(self){
            self.tableView?.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
        }
        if !textField.text!.isEmpty || textField.text != ""{
            oldQuantity = (textField.text! as NSString).integerValue
        }
        
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        selectedTextField = nil
        if textField.text!.isEmpty || textField.text == ""{
            textField.text = "1"
            AlertManager.showAlert(nil, title: "Error", message: "Quantity can not be empty", buttonNames: nil, completion: nil)
        }else{
            let q: Int = (textField.text! as NSString).integerValue
            if q < 1{
                textField.text = "1"
                AlertManager.showAlert(nil, title: "Error", message: "Quantity can not be less than 1", buttonNames: nil, completion: nil)
            }else{
                var qPrice: Float!
                if let price: NSString = self.data!.objectForKey("menu_price") as? NSString{
                    qPrice = price.floatValue
                }else if let price: NSString = self.data!.objectForKey("price") as? NSString{
                    qPrice = price.floatValue
                }
                delegate?.onQuantityChange(self.tableView!.indexPathForCell(self)!, oldValue:oldQuantity!, newValue: q, price: qPrice)
            }
        }
    }
    
    func onToolbarDoneTap(){
        self.selectedTextField?.resignFirstResponder()
    }

}
