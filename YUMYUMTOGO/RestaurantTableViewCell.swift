//
//  RestaurantTableViewCell.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/22/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    
    @IBOutlet var thumbImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var typeLabel: CellTypeLabel!
    @IBOutlet var cuisineLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    
    convenience init() {
        self.init()
    }
    
    convenience init(frame: CGRect) {
        self.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.clipsToBounds = true
        self.backgroundColor = UIColor.clearColor()
        //self.contentView.backgroundColor = UIColor(rgba: "fb6c64")
        //self.shortDescription.textContainer.lineFragmentPadding = 0
        //self.shortDescription.textContainerInset = UIEdgeInsetsZero
        thumbImage.contentMode = UIViewContentMode.ScaleAspectFill
        typeLabel.layer.borderColor = UIColor.whiteColor().CGColor
        typeLabel.layer.borderWidth = 1
        typeLabel.layer.cornerRadius = 8
        typeLabel.clipsToBounds = true
        
        //contentView.addConstraint(NSLayoutConstraint(item: typeLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: typeLabel.frame.size.width + 10))
        
        let maskLayer: CALayer = CALayer()
        maskLayer.frame = thumbImage.bounds
        maskLayer.shadowRadius = 10
        //maskLayer.shadowPath = CGPathCreateWithRoundedRect(CGRectInset(imageview.bounds, 5, 5), 10, 10, nil)
        //println(thumbImage.frame.size.width)
        maskLayer.shadowPath = CGPathCreateWithRect(CGRectMake(-15, -5, thumbImage.frame.size.width - 10, thumbImage.frame.size.height + 30), nil)
        //maskLayer.shadowPath = CGPathCreateWithRect(CGRectInset(imageview.bounds, 5, 5), nil)
        maskLayer.shadowOpacity = 1
        maskLayer.shadowOffset = CGSizeZero
        maskLayer.shadowColor = UIColor.whiteColor().CGColor
        
        thumbImage.layer.mask = maskLayer
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
