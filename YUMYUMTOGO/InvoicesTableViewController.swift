//
//  InvoicesTableViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 5/17/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit

protocol InvoicesTableViewControllerDelegate{
    func onMenuButtonTap()
}

class InvoicesTableViewController: UITableViewController {
    
    var delegate: InvoicesTableViewControllerDelegate?
    
    var tableData: NSMutableArray = NSMutableArray()
    
    var activityIndicatorParentView: AnyObject!
    var noDataMessageLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Invoices"
        let refreshButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Refresh, target: self, action: "onReloadTap:")
        self.navigationItem.rightBarButtonItem = refreshButton
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: "onMenuTap:")
        
        let indicatorSuperview = self.parentViewController?.view.window != nil ? self.parentViewController?.view.window! : (self.view.window != nil ? self.view.window! : self.view)
        if activityIndicatorParentView == nil{
            activityIndicatorParentView = indicatorSuperview
        }
        
        loadTableData()
        updateDeviceBadge()

    }
    
    func onReloadTap(sender: UIBarButtonItem){
        self.loadTableData()
        updateDeviceBadge()
    }
    
    // MARK: - Menu Button Tap
    
    func onMenuTap(sender: UIBarButtonItem){
        print("here")
        self.delegate?.onMenuButtonTap()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.tableData.count == 0{
            self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width,
                self.tableView.bounds.size.height))
            
            self.noDataMessageLabel.text = "No data is avaible"
            //center the text
            self.noDataMessageLabel.textAlignment = NSTextAlignment.Center
            //auto size the text
            self.noDataMessageLabel.sizeToFit()
            //set back to label view
            self.tableView.backgroundView = self.noDataMessageLabel
            return 0
        }else{
            noDataMessageLabel.text = ""
            if self.noDataMessageLabel != nil{
                self.noDataMessageLabel.removeFromSuperview()
            }
        }
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 
        cell.accessoryType = UITableViewCellAccessoryType.DetailDisclosureButton
        //cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator

        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        cell.textLabel?.text = "ID: " + (cellData.objectForKey("invoice_id") as? String!)!
        cell.detailTextLabel?.text = cellData.objectForKey("printstatus") as? String
        

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        let vc: InvoiceDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("InvoiceDetailViewController") as! InvoiceDetailViewController
        if let filename: String = cellData.objectForKey("fileName") as? String{
            if filename != ""{
                vc.navigationItem.title = cellData.objectForKey("invoice_id") as? String
                vc.link = NSURL(string: filename)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        let activityIndicator = UICustomActivityView()
        activityIndicator.message = "Loading"
        activityIndicator.showActivityIndicator(activityIndicatorParentView, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        let ID: String = cellData.objectForKey("id") as! String
        DataManager.loadDataAsyncWithCallback("invoices/lists/\(ID)", completion: { (data, error) -> Void in
            //println(data)
            //println(error)
            dispatch_async(dispatch_get_main_queue()){
                activityIndicator.hideActivityIndicator()
                if error == nil && data != nil{
                    //println(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    if let response: NSArray = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSArray{
                        let detailData: NSDictionary = response[0] as! NSDictionary
                        let dateFormatter: NSDateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        print(detailData.objectForKey("createTime"))
                        let creationDate: NSDate = NSDate(timeIntervalSince1970: (detailData.objectForKey("createTime") as! NSString).doubleValue / 1000 as NSTimeInterval)
                        var message: String = "Created on: " + dateFormatter.stringFromDate(creationDate)
                        if detailData.objectForKey("printstatus") as? String == "DONE"{
                            print(detailData.objectForKey("updateTime"))
                            let updateTime: NSDate = NSDate(timeIntervalSince1970: (detailData.objectForKey("updateTime") as! NSString).doubleValue / 1000 as NSTimeInterval)
                            message = message + "\nPrinted on: " + dateFormatter.stringFromDate(updateTime)
                        }
                        AlertManager.showAlert(self, title: "Print Details", message: message, buttonNames: nil, completion: nil)
                    }
                }else if error != nil{
                    AlertManager.showAlert(self, title: "Error", message: "\(error!.localizedDescription)", buttonNames: nil, completion: nil)
                }
            }
        })
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UIStatusBarStyle
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    // MARK: - loadTableData
    func loadTableData(){
        let activityIndicator = UICustomActivityView()
        activityIndicator.message = "Loading"
        activityIndicator.showActivityIndicator(activityIndicatorParentView, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        DataManager.loadDataAsyncWithCallback("invoices/lists", completion: { (data, error) -> Void in
            //println(data)
            //println(error)
            dispatch_async(dispatch_get_main_queue()){
                activityIndicator.hideActivityIndicator()
                if error == nil && data != nil{
                    //println(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    if let response: NSArray = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSArray{
                        self.tableData.removeAllObjects()
                        self.tableData = NSMutableArray(array: response)
                        self.tableView.reloadData()
                    }else{
                        self.tableData.removeAllObjects()
                        self.tableView.reloadData()
                    }
                }else if error != nil{
                    AlertManager.showAlert(self, title: "Error", message: "\(error!.localizedDescription)", buttonNames: nil, completion: nil)
                }
            }
        })
    }
    
    // MARK: - updateDeviceBadge
    func updateDeviceBadge(){
        let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let user_id: String = prefs.objectForKey("userID") as! String
        if let nDeviceToken: String = prefs.objectForKey("deviceToken") as? String{
            let infoDictionary: NSDictionary = NSBundle.mainBundle().infoDictionary!
            let applicationVersion: String = (infoDictionary.objectForKey("CFBundleShortVersionString") as! String)
            let appBuild: String = infoDictionary.objectForKey("CFBundleVersion") as! String
            
            let postData: NSDictionary = ["Device": ["user_id": user_id, "uuid": nDeviceToken, "appversion": applicationVersion, "build": appBuild, "badge": 0]]
            print(postData)
            
            DataManager.postDataAsyncWithCallback("devices/add", data: postData, json: true) { (data, error) -> Void in
                if error == nil && data != nil{
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                }else{
                    print(data)
                    print("\(error), \(error?.localizedDescription)")
                }
            }
        }
    }

}
