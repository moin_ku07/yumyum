//
//  RestaurantTableViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 1/21/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreLocation

protocol RestaurantTableViewControllerDelegate{
    func onTableDataUpdate(tableData: NSMutableArray)
    func didSelectRowAtIndex(index: Int, rowdata: NSDictionary?)
}

class RestaurantTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    var delegate: RestaurantTableViewControllerDelegate?
    
    var locationManager: CLLocationManager = CLLocationManager()
    var userLocation: CLLocationCoordinate2D?
    
    var tableData: NSMutableArray = NSMutableArray(){
        didSet{
            self.delegate?.onTableDataUpdate(self.tableData)
        }
    }
    
    var selectedIndexPath: NSIndexPath? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.activityType = CLActivityType.OtherNavigation
        locationManager.pausesLocationUpdatesAutomatically = true
        if AlertManager.isIOS8(){
            if #available(iOS 8.0, *) {
                self.locationManager.requestWhenInUseAuthorization()
            } else {
                // Fallback on earlier versions
            }//NSLocationWhenInUseUsageDescription
            //self.locationManager.requestAlwaysAuthorization()//NSLocationAlwaysUsageDescription
        }
        
        self.view.backgroundColor = UIColor(rgba: "ff3b30")
        self.tableView.backgroundColor = UIColor(rgba: "ff3b30")
        
        self.tableView.rowHeight = 112
        self.tableView.registerNib(UINib(nibName: "RestaurantTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        //self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tableView.separatorInset = UIEdgeInsetsZero
        
        dispatch_async(dispatch_get_main_queue()){
            self.loadRestaurantsData()
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        dispatch_async(dispatch_get_main_queue()){
            self.tableView.contentInset = UIEdgeInsetsZero
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: RestaurantTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! RestaurantTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        if #available(iOS 8.0, *) {
            cell.layoutMargins = UIEdgeInsetsZero
            cell.preservesSuperviewLayoutMargins = false
        }
        
        let cellData: NSDictionary = self.tableData.objectAtIndex(indexPath.row) as! NSDictionary
        
        cell.nameLabel.text = cellData.objectForKey("name") as! String!
        
        if indexPath.row % 2 == 0{
            cell.typeLabel.backgroundColor = UIColor(rgba: "ec8025")
        }else{
            cell.typeLabel.backgroundColor = UIColor(rgba: "50ADB4")
        }
        if let typeText: String = cellData.objectForKey("type") as? String{
            if typeText.isEmpty || typeText == ""{
                cell.typeLabel.hidden = true
            }else{
                cell.typeLabel.hidden = false
            }
            cell.typeLabel.text = "\(typeText)\u{2007}\u{2007}"
        }else{
            cell.typeLabel.hidden = true
        }
        if let cusineText: String = cellData.objectForKey("cuisine") as? String{
            if cusineText.isEmpty || cusineText == ""{
                cell.cuisineLabel.hidden = true
            }else{
                cell.cuisineLabel.hidden = false
            }
            cell.cuisineLabel.text = cusineText
        }else{
            cell.cuisineLabel.hidden = true
        }
        
        if userLocation != nil{
            let latString = cellData.objectForKey("lat") as! String
            let lngString = cellData.objectForKey("lng") as! String
            let latTrim: NSString = latString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let lngTrim: NSString = lngString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let lat: Double = latTrim.doubleValue
            let lng: Double = lngTrim.doubleValue
            let restaurantCoordinate: CLLocation? = CLLocation(latitude: lat, longitude: lng)
            if restaurantCoordinate != nil{
                if let distance: CLLocationDistance = restaurantCoordinate?.distanceFromLocation(CLLocation(latitude: self.userLocation!.latitude, longitude: self.userLocation!.longitude)){
                    let distanceInMile: Double = (distance / 1000) * 0.621371
                    let formatter: NSNumberFormatter = NSNumberFormatter()
                    formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                    formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
                    formatter.maximumFractionDigits = 2
                    cell.distanceLabel.text = "\(formatter.stringFromNumber(distanceInMile)!) MI"
                    cell.distanceLabel.hidden = false
                }
            }
        }else{
            cell.distanceLabel.hidden = true
        }
        /*
        let queue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
        dispatch_async(queue){
            let url: String = DataManager.domain().root + "img/restaurants/thumb/" + (cellData.objectForKey("thumb") as String)
            //println(url)
            var error: NSError?
            if let imageData: NSData = NSData(contentsOfURL: NSURL(string: url)!, options: NSDataReadingOptions.allZeros, error: &error){
                dispatch_async(dispatch_get_main_queue()){
                    cell.thumbImage.image = UIImage(data: imageData)
                }
            }
        }
        */
        let url: String = DataManager.domain().root + "img/restaurants/thumb/" + (cellData.objectForKey("thumb") as! String)
        cell.thumbImage.sd_setImageWithURL(NSURL(string: url)!, placeholderImage: UIImage(named: "take-photo"))

        return cell
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if selectedIndexPath != nil{
            let cell: RestaurantTableViewCell? = tableView.cellForRowAtIndexPath(selectedIndexPath!) as? RestaurantTableViewCell
            cell?.contentView.layer.borderColor = UIColor.clearColor().CGColor
            cell?.contentView.layer.borderWidth = 0
        }
        let cell: RestaurantTableViewCell = tableView.cellForRowAtIndexPath(indexPath) as! RestaurantTableViewCell
        cell.contentView.layer.borderColor = UIColor.whiteColor().CGColor
        cell.contentView.layer.borderWidth = 1
        selectedIndexPath = indexPath
                
        return true
    }
    
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        if selectedIndexPath != nil{
            let cell: RestaurantTableViewCell = tableView.cellForRowAtIndexPath(selectedIndexPath!) as! RestaurantTableViewCell
            cell.contentView.layer.borderColor = UIColor.clearColor().CGColor
            cell.contentView.layer.borderWidth = 0
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = self.tableData.objectAtIndex(indexPath.row) as! NSDictionary
        delegate?.didSelectRowAtIndex(indexPath.row, rowdata: cellData)
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - CLLocationManager
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if #available(iOS 8.0, *) {
            if status == CLAuthorizationStatus.AuthorizedAlways || status == CLAuthorizationStatus.AuthorizedWhenInUse{
                print("Authorized")
                locationManager.startUpdatingLocation()
                if locationManager.location != nil{
                    self.userLocation = locationManager.location!.coordinate
                }
            }else if status == CLAuthorizationStatus.Denied{
                AlertManager.showAlert(self, title: "Alert", message: "You have denied location access. You can change it from Settings->Privacy->Location->YUM YUM TO GO", buttonNames: nil, completion: nil)
            }else if status == CLAuthorizationStatus.Restricted{
                AlertManager.showAlert(self, title: "Alert", message: "You have restricted location access. You can change it from Settings->Privacy->Location->YUM YUM TO GO", buttonNames: nil, completion: nil)
            }else if status == CLAuthorizationStatus.NotDetermined{
                print("not determined")
                locationManager.requestWhenInUseAuthorization()
            }else{
                print("else")
                print(status.rawValue)
            }
        } else {
            if status == CLAuthorizationStatus.Authorized{
                print("Authorized")
                locationManager.startUpdatingLocation()
                if locationManager.location != nil{
                    self.userLocation = locationManager.location!.coordinate
                }
            }else if status == CLAuthorizationStatus.Denied{
                AlertManager.showAlert(self, title: "Alert", message: "You have denied location access. You can change it from Settings->Privacy->Location->YUM YUM TO GO", buttonNames: nil, completion: nil)
            }else if status == CLAuthorizationStatus.Restricted{
                AlertManager.showAlert(self, title: "Alert", message: "You have restricted location access. You can change it from Settings->Privacy->Location->YUM YUM TO GO", buttonNames: nil, completion: nil)
            }else if status == CLAuthorizationStatus.NotDetermined{
                print("not determined")
                locationManager.startUpdatingLocation()
            }else{
                print("else")
                print(status.rawValue)
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //println(locations)
        if let location = locations.first{
            self.userLocation = location.coordinate
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("didFailWithError: \(error.localizedDescription)")
        //locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didFinishDeferredUpdatesWithError error: NSError?) {
        print("didFinishDeferredUpdatesWithError: \(error?.localizedDescription)")
        //locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - Load Restaurants Data
    func loadRestaurantsData(){
        let activityIndicator = UICustomActivityView()
        let indicatorSuperview = self.parentViewController?.view.window != nil ? self.parentViewController?.view.window! : (self.view.window != nil ? self.view.window! : self.view)
        activityIndicator.showActivityIndicator(indicatorSuperview!, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        DataManager.loadDataAsyncWithCallback("restaurants/lists", completion: { (data, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                activityIndicator.hideActivityIndicator()
                if error == nil && data != nil{
                    if let jsonData: NSArray = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSArray{
                        if jsonData.count > 0{
                            self.tableData.removeAllObjects()
                            for value in jsonData{
                                self.tableData.addObject(value as! NSDictionary)
                            }
                            self.tableView.reloadData()
                            self.delegate?.onTableDataUpdate(self.tableData)
                        }
                    }
                }
            })
        })
    }

}
