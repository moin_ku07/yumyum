//
//  MenusCollectionViewCell.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 2/7/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit
import QuartzCore

class MenusCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var thumbImage: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    convenience init() {
        self.init()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView(){
        //self.backgroundColor = UIColor(rgba: "4e4e5a")
        
        let layer: CALayer = CALayer()
        //layer.bounds = CGRectMake(0, 0, self.bounds.size.width, 3)
        //layer.position = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height)
        //layer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8).CGColor
        layer.zPosition = -5
        layer.shadowOpacity = 1
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowRadius = 2
        layer.shadowOffset = CGSizeMake(0, 3)
        layer.shadowPath = UIBezierPath(rect: CGRectMake(0, self.bounds.size.height - 3, self.bounds.size.width, 3)).CGPath
        //println(CGRectMake(0, self.bounds.size.height, self.bounds.size.width, 3))
        
        
        //println(self.bounds)
        
        //self.layer.sublayers.removeAll(keepCapacity: false)
        self.layer.addSublayer(layer)

        self.clipsToBounds = false
        
        //self.layer.borderColor = UIColor.blackColor().CGColor
        //self.layer.borderWidth = 1
        
        /*self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSizeMake(0, 3)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 1.0*/
        
        cameraButton.layer.borderColor = UIColor.grayColor().CGColor
        cameraButton.layer.borderWidth = 1
        cameraButton.layer.cornerRadius = 4
        cameraButton.clipsToBounds = true
        cameraButton.alpha = 0.5

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }
    
}
