//
//  AddonsViewController.swift
//  YUM YUM TO GO
//
//  Created by Moin Uddin on 2/5/15.
//  Copyright (c) 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreData

class AddonsViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, LoginViewControllerDelegate {
    
    @IBOutlet var banner: UIImageView!
    @IBOutlet var shortDescription: UITextView!
    @IBOutlet var priceLabel: CellTypeLabel!
    @IBOutlet var quantityContainer: UIView!
    @IBOutlet var menuQuantity: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var noItemLabel: UILabel!
    
    var menuSlider: UISlider!
    
    var menuData: NSDictionary?
    var restaurant_id: String?
    var taxValue: String?
    var restaurant_name: String?
    
    var tableData: NSMutableArray = NSMutableArray()
    
    var selectedAddons: NSMutableArray = NSMutableArray()
    
    var activityIndicatorParentView: AnyObject!
    
    var currentTimestamp: String {
        get {
            return "\(NSDate().timeIntervalSince1970 * 1000)"
        }
    }
    
    let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let indicatorSuperview = self.parentViewController?.view.window != nil ? self.parentViewController?.view.window! : (self.view.window != nil ? self.view.window! : self.view)
        if activityIndicatorParentView == nil{
            activityIndicatorParentView = indicatorSuperview
        }
        
        self.view.backgroundColor = UIColor(rgba: "ff3b30")
        
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.tintColor = UIColor(rgba: "ffffff")
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "ff3b30")
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(20)]
        self.navigationItem.hidesBackButton = false
        
        
        let addToCartButton: UIBarButtonItem = UIBarButtonItem()
        addToCartButton.image = UIImage(named: "icon-cart-add")
        addToCartButton.target = self
        addToCartButton.action = "onAddToCartTap:"
        navigationItem.rightBarButtonItem = addToCartButton
        
        // style quantityContainer
        quantityContainer.layer.borderColor = UIColor.whiteColor().CGColor
        quantityContainer.layer.borderWidth = 1
        // style menuQuantity
        //menuQuantity.layer.borderWidth = 1
        //menuQuantity.layer.borderColor = UIColor.whiteColor().CGColor
        menuQuantity.delegate = self
        
        /*
        menuSlider = UISlider(frame: CGRectMake(0, 0, 120, 31))
        //menuSlider.layer.anchorPoint = CGPointMake(0, 0)
        //menuSlider.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
        menuSlider.minimumValue = 1
        menuSlider.value = 1
        menuSlider.maximumValue = 100
        menuSlider.enabled = true
        menuSlider.userInteractionEnabled = true
        menuSlider.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.view.addSubview(menuSlider)
        
        self.view.addConstraint(NSLayoutConstraint(item: menuSlider, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 30))
        self.view.addConstraint(NSLayoutConstraint(item: menuSlider, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: self.banner, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: -30))
        //self.view.addConstraint(NSLayoutConstraint(item: menuSlider, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.banner, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.banner, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: menuSlider, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.banner, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: menuSlider, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 8))
        
        menuSlider.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
        */
        
        //self.tableView.contentInset = UIEdgeInsetsMake(150, 0, 0, 0)
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        if menuData != nil{
            navigationItem.title = menuData?.objectForKey("name") as! String!
            
            if let pricetext: NSString = menuData?.objectForKey("price") as? NSString{
                // priceLabel style
                priceLabel.layer.borderColor = UIColor.whiteColor().CGColor
                priceLabel.layer.borderWidth = 1
                //priceLabel.layer.cornerRadius = 8
                priceLabel.clipsToBounds = true
                priceLabel.backgroundColor = UIColor(rgba: "50ADB4")
                let formatter: NSNumberFormatter = NSNumberFormatter()
                formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
                formatter.maximumFractionDigits = 2
                priceLabel.text = "$\(formatter.stringFromNumber(pricetext.doubleValue)!)\u{2007}\u{2007}"
            }
            
            if let shorttext: String = menuData?.objectForKey("description") as? String{
                //self.tableView.contentInset = UIEdgeInsetsMake(200, 0, 0, 0)
                self.shortDescription.text = shorttext
                //self.shortDescription.textColor = UIColor.blackColor()
                self.shortDescription.textContainer.lineFragmentPadding = 0
                self.shortDescription.textContainerInset = UIEdgeInsetsZero
            }
            
            let queue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
            dispatch_async(queue){
                let url: String = DataManager.domain().root + "img/menus/banner/" + (self.menuData?.objectForKey("banner") as! String)
                //println(url)
                var error: NSError?
                do {
                    let imageData: NSData = try NSData(contentsOfURL: NSURL(string: url)!, options: NSDataReadingOptions())
                    dispatch_async(dispatch_get_main_queue()){
                        self.banner.image = UIImage(data: imageData)
                    }
                } catch let error1 as NSError {
                    error = error1
                } catch {
                    fatalError()
                }
            }
            loadAddonsData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return tableData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: AddonsTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! AddonsTableViewCell
        
        let cellData: NSDictionary = self.tableData.objectAtIndex(indexPath.row) as! NSDictionary
        
        cell.title.text = cellData.objectForKey("name") as? String
        cell.price.text = "$" + (cellData.objectForKey("price") as! String)
        
        cell.switchView.addTarget(self, action: "onSwitchValueChange:", forControlEvents: UIControlEvents.ValueChanged)
        
        return cell
    }
    
    // MARK: - onSwitchValueChange
    
    func onSwitchValueChange(sender: UISwitch){
        var cell: AddonsTableViewCell!
        if let cell1: AddonsTableViewCell = sender.superview?.superview?.superview as? AddonsTableViewCell{
            cell = cell1
        }else if let cell1:AddonsTableViewCell = sender.superview?.superview as? AddonsTableViewCell{
            cell = cell1
        }
        if let indexPath: NSIndexPath = self.tableView.indexPathForCell(cell){
            if let cellData: NSDictionary = tableData.objectAtIndex(indexPath.row) as? NSDictionary{
                //println(cellData)
                let addonID: Int = Int((cellData.objectForKey("id") as! String))!
                let addonDict: NSMutableDictionary = ["id": cellData.objectForKey("id") as! String, "name": cellData.objectForKey("name") as! String, "price": cellData.objectForKey("price") as! String, "quantity": "1"]
                if sender.on{
                    //selectedAddons.addObject(addonID)
                    selectedAddons.addObject(addonDict)
                }else{
                    //selectedAddons.removeObject(addonID)
                    selectedAddons.removeObject(addonDict)
                }
                //println(selectedAddons.indexOfObject(addonID))
                //println(selectedAddons)
            }
        }
        
    }
    
    // MARK: - UIStatusBarStyle
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    // MARK: - Textfield delegate
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        // Create a button bar for the number pad
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        
        // Setup the buttons to be put in the system.
        var item: UIBarButtonItem = UIBarButtonItem()
        item = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("quantityDoneTap") )
        let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let toolbarButtons = [flexSpace,item]
        
        //Put the buttons into the ToolBar and display the tool bar
        keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
        textField.inputAccessoryView = keyboardDoneButtonView
        
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if Int(textField.text!) < 1{
            textField.text = "1"
            AlertManager.showAlert(self, title: "Alert", message: "Quantity can not be less than 1", buttonNames: nil, completion: nil)
        }
    }
    
    func quantityDoneTap(){
        menuQuantity.resignFirstResponder()
    }
    
    // MARK: - LoginViewControllerDelegate
    func didLogggeIn(success: Bool) {
        if success == true{
            if let nvc: NavigationController = self.navigationController as? NavigationController{
                //println("here1")
                nvc.updateSidebarMenu()
            }
            self.addToCart()
        }
    }
    
    // MARK: - onAddToCartTap
    func onAddToCartTap(sender: AnyObject){
        if isLoggedIn == false{
            AlertManager.showAlert(self, title: "Warning!", message: "Log in is required for this action", buttonNames: ["Cancel", "Login"], completion: { (index) -> Void in
                if index == 1{
                    let loginVC: LoginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("loginViewController") as! LoginViewController
                    loginVC.delegate = self
                    loginVC.shouldHideOnLogin = true
                    self.presentViewController(loginVC, animated: true, completion: nil)
                }
            })
        }else{
            self.addToCart()
        }
    }
    
    // MARK: - addToCart
    func addToCart(){
        print("currentCartRestaurantId: \(currentCartRestaurantId) & restaurant_id: \(restaurant_id)")
        if currentCartRestaurantId != nil && currentCartRestaurantId != restaurant_id{
            AlertManager.showAlert(self, title: "Warning", message: "Menu for different restaurants can not be added to cart.", buttonNames: nil, completion: nil)
        }else{
            currentCartRestaurantId = restaurant_id
            prefs.setValue(taxValue!, forKey: "tax")
            prefs.setValue(restaurant_name!, forKey: "restaurant_name")
            prefs.synchronize()
            let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("database")
            let cartItem: Carts = CoreDataHelper.insertManagedObject(NSStringFromClass(Carts), managedObjectContext: moc) as! Carts
            cartItem.id = currentTimestamp
            cartItem.menu_id = menuData?.objectForKey("id") as! String!
            cartItem.menu_name = menuData?.objectForKey("name") as! String!
            cartItem.menu_price = menuData?.objectForKey("price") as! String!
            cartItem.quantity = menuQuantity.text!
            cartItem.restaurant_id = restaurant_id!
            if selectedAddons.count > 0{
                for saddon in selectedAddons{
                    let addonDict: NSMutableDictionary = saddon as! NSMutableDictionary
                    addonDict.setValue(menuQuantity.text, forKey: "quantity")
                }
            }
            if let addon: NSData = try? NSJSONSerialization.dataWithJSONObject(selectedAddons, options: NSJSONWritingOptions()){
                cartItem.addons = addon.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
            }else{
                let addons: NSArray = []
                if let addonData: NSData = try? NSJSONSerialization.dataWithJSONObject(addons, options: NSJSONWritingOptions()){
                    cartItem.addons = addonData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
                }else{
                    cartItem.addons = "[]"
                }
            }
            let success: Bool = CoreDataHelper.saveManagedObjectContext(moc)
            if success{
                AlertManager.showAlert(self, title: "Success", message: "Menu has been added to cart", buttonNames: nil, completion: nil)
            }else{
                AlertManager.showAlert(self, title: "Error", message: "Menu was not added to cart. Please try again.", buttonNames: nil, completion: nil)
            }
        }
        /*
        let postData: NSDictionary = ["Cart": ["menu_id": menuData?.objectForKey("id") as String!, "quantity": menuQuantity.text, "addons": selectedAddons]]
        println(postData)
        let activityIndicator = UICustomActivityView()
        activityIndicator.showActivityIndicator(activityIndicatorParentView, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        DataManager.postDataAsyncWithCallback("carts/add", data: postData, json: true) { (data, error) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                activityIndicator.hideActivityIndicator()
                if data != nil && error == nil{
                    if let response: NSDictionary = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.allZeros, error: nil) as? NSDictionary{
                        if response.objectForKey("success") as Bool == true{
                            AlertManager.showAlert(self, title: "Success", message: "Item has been added to cart", buttonNames: nil, completion: nil)
                        }else{
                            AlertManager.showAlert(self, title: "Error", message: "Item was not added to cart. Please try again.", buttonNames: nil, completion: nil)
                        }
                    }
                }else if error != nil{
                    println(error!.localizedDescription)
                    AlertManager.showAlert(self, title: "Error", message: error!.localizedDescription, buttonNames: nil, completion: nil)
                }
            }
        }*/
    }
    
    // MARK: - Load Addons Data
    func loadAddonsData(){
        let activityIndicator = UICustomActivityView()
        activityIndicator.showActivityIndicator(activityIndicatorParentView, style: UIActivityIndicatorViewStyle.Gray, shouldHaveContainer: false)
        let url: String = "addons/lists?menu_id=" + (menuData?.objectForKey("id") as! String!)
        //println(url)
        DataManager.loadDataAsyncWithCallback(url, completion: { (data, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                activityIndicator.hideActivityIndicator()
                if error == nil && data != nil{
                    if let jsonData: NSArray = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())) as? NSArray{
                        if jsonData.count > 0{
                            self.tableData.removeAllObjects()
                            for value in jsonData{
                                self.tableData.addObject(value as! NSDictionary)
                            }
                            if self.tableData.count > 0{
                                self.noItemLabel.hidden = true
                                self.tableView.reloadData()
                            }else{
                                self.noItemLabel.hidden = false
                                self.view.bringSubviewToFront(self.noItemLabel)
                            }
                        }
                    }else{
                        self.noItemLabel.hidden = false
                        self.view.bringSubviewToFront(self.noItemLabel)
                    }
                }else{
                    self.noItemLabel.hidden = false
                    self.view.bringSubviewToFront(self.noItemLabel)
                }
            })
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
